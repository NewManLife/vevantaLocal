<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package VueGalery
 */

get_header();
?>
	<div id="app">
		<main id="primary" class="site-main">
      <form action="" method="POST">
          <input  name="getText" type="submit" value="Получить рандомный текст" />
      </form>
      <?php

        //Подключаем файл класса
        require_once "CombineMorph/CombineMorph.php";

        //Текст из которого нужно получить варианты

        $text = "На Vevanta.ru вы {можете|cможете} купить {КЛАСС ИЗ БАЗЫ} в Тюмени, 
        а также {подобрать|посмотреть|увидеть|оценить} любой интересующий вас {объект недвижимости|объект жилья|объект} 
        в Тюмени и Тюменской области.
        Vevanta.ru предоставляет {фото-тур|видео-тур|автобусный-тур} на {объект недвижимости|объект жилья}
        который {представлен|присутствует|находятся} в базе обьявлений.
        почти {100|99|98|97|96|95} процентов {клиентов|покупателей} {довольны|удовлетворенны} 
        {сделанным выбором|принятым решением} по {покупке|приобретению} {объекта недвижимости|объекта жилья|недвижимости|жилья}.
        Для {оргазации|подтверждения|бронирования} тура на {УЛИЦА ИЗ БАЗЫ или НАСЕЛЕННЫЙ ПУНКТ ИЗ БАЗЫ или ТРАКТ ИЗ БАЗЫ} 
        вам {нужно|необходимо} связаться с нами по телефону +7(3452) 69-23-47.
        Персональный риелтор {подберет для вас|предложит вам|посоветует вам} {оптимальные|лучшие|хорошие} условия
        для {приобретения|покупки|аренды} {объекта недвижимости|объекта жилья|недвижимости|жилья} .
        Найдено {4,7 тыс. квартир ИЗ БАЗЫ} на {УЛИЦА ИЗ БАЗЫ или НАСЕЛЕННЫЙ ПУНКТ ИЗ БАЗЫ или ТРАКТ ИЗ БАЗЫ} в городе Тюмень.
        Минимальная цена {КЛАСС ИЗ БАЗЫ} - {2 млн. руб.|3 млн. руб.|4 млн. руб.|5 млн. руб.} ИЗ БАЗЫ.
        Максимальная цена {КЛАСС ИЗ БАЗЫ} - {10 млн. руб.|15 млн. руб.|20 млн. руб.} ИЗ БАЗЫ.
        Минимальная площадь - {25 м2|50 м2|75 м2} ИЗ БАЗЫ. Максимальная площадь - {150 м2|200 м2|250 м2} ИЗ БАЗЫ.";
     
        //Создадим объект класса, передав в качестве параметра текст
        $combineMorph = new CombineMorph($text);
        //Получим количество возможных вариантов:
        $variantsCount = $combineMorph->getVariantsCount();
        
        //Сформируем 100 случайных вариантов текста:
        for($i=0;$i<100;$i++)
        {
            $randomTextVariant[$i] = $combineMorph->getRandomVariant();
        }

        $randomText = $combineMorph->getRandomVariant();
        if (isset($_POST['getText']))
        {
        echo "<h3>$randomText</h3>";
         };
        
        ?>

			<?php
			if ( have_posts() ) :

				if ( is_home() && ! is_front_page() ) :
					?>
					<header>
						<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
					</header>
					<?php
				endif;

				/* Start the Loop */
				while ( have_posts() ) :
					the_post();

					/*
					* Include the Post-Type-specific template for the content.
					* If you want to override this in a child theme, then include a file
					* called content-___.php (where ___ is the Post Type name) and that will be used instead.
					*/
					get_template_part( 'template-parts/content', get_post_type() );

				endwhile;

				the_posts_navigation();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif;
      ?>
      
        

		</main><!-- #main -->
      <modal-default ref="modalDefault"></modal-default>
        
       <section class="" >

        <!-- <Vevanta-Video></Vevanta-Video> -->
         <!-- <for-what></for-what> -->
         <!-- <Ipoteka-Top></Ipoteka-Top> -->
        <!-- <for-what></for-what> -->
        <Call-Back-Consultation></Call-Back-Consultation>

        <!-- <Call-Back-Hero></Call-Back-Hero>
         <Form-Two></Form-Two>
		 <Ipoteka></Ipoteka>
         <How-It-Works></How-It-Works> -->
		 <!-- <New-House-List></New-House-List> -->

         
         
         <!-- <Search-Filter-Home></Search-Filter-Home>
         <Listing-Items></Listing-Items>
         <Objects-By-Parameters></Objects-By-Parameters>
         <New-House-List></New-House-List>
         
         
        
        
        
        <Filter-Items></Filter-Items>
        
        
        
        <Search-Filter-Mobile></Search-Filter-Mobile>
        
        
        
        
        <nw-Top-List></nw-Top-List>
        
        <Plans-New-House></Plans-New-House>
        <Calc-House></Calc-House>
        <life-Houses></life-Houses>

        <Developer-Map></Developer-Map>

        <Ipoteka></Ipoteka>
        <Ipoteka-Calc></Ipoteka-Calc>

        <Consultation></Consultation>
        
        <Button-Modal></Button-Modal>
        <Form-Phone></Form-Phone>
        
        
        <New-House-Card></New-House-Card> 

        <Tile-View></Tile-View>

        <Item></Item>


        <Slider-Personal></Slider-Personal>
        <Quiz-Project></Quiz-Project>
        <Quiz-1></Quiz-1>
        <Quiz-Apartment-Parameters></Quiz-Apartment-Parameters>
        <Quiz-Country-House-Parameters></Quiz-Country-House-Parameters>
        <Order-Quiz></Order-Quiz>
        
        <Houses-Map></Houses-Map>

        <Filter></Filter>

        <Manager-Item></Manager-Item>
        

        <Slider-Personal></Slider-Personal>Вызов?
        <multi-Select></multi-Select>Вызов?
        <Tile-View></Tile-View>Вызов?
        <Pagination></Pagination> Вызов?
        <List-View></List-View> Вызов?
        <Info-Flat></Info-Flat> Вызов?
        <Item></Item> Вызов?
        <Modal-Default></Modal-Default> Вызов?
        <Modal-Success></Modal-Success> Вызов?
        <Personal></Personal> Вызов?
        <Object-Helper></Object-Helper> Вызов?
        <Galery></Galery> Вызов? -->

       

      </section>
    </div>
	

<?php
// get_sidebar();
get_footer();
