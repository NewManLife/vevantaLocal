<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package VueGalery
 */

?>

	<!-- <footer id="colophon" class="site-footer">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'vuegalery' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'vuegalery' ), 'WordPress' );
				?>
			</a>
			<span class="sep"> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'vuegalery' ), 'vuegalery', '<a href="http://underscores.me/">Underscores.me</a>' );
				?>
		</div>
	</footer> -->
</div><!-- #page -->

<?php wp_footer(); ?>
	<script src=https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/js/uikit.min.js></script>
    <script src=https://cdn.jsdelivr.net/npm/uikit@3.5.5/dist/js/uikit-icons.min.js></script>
    <script src="http://localhost/wordpress/wp-content/themes/vuegalery/cn_vevanta/dist/js/chunk-vendors.1bebe360.js"></script>
    <script src="http://localhost/wordpress/wp-content/themes/vuegalery/cn_vevanta/dist/js/app.8356ac99.js"></script>
</body>
</html>
