<?php
	$ancestors = get_ancestors(get_the_ID(), 'page' );
	$page_template_slug = get_page_template_slug( get_the_ID() );
	$options = [];
 	//> SEO
	$options[] = [
		[
      'type'    => 'box',
      'title'   => __('SEO', 'fw1'),
      'attr'    => ['class' => 'custom-class', 'data-foo' => 'bar'],
      'options' => [
				'title_seo'  => [
					'type' => 'text',
					'label' => 'Title',
					'max' => 2,
					'fw-storage' => [
						'type' => 'post-meta',
						'post-meta' => 'title_seo',
					],
				],
        'description_seo'  => [
					'type' => 'text',
					'label' => 'Description',
					'fw-storage' => [
						'type' => 'post-meta',
						'post-meta' => 'description_seo',
					],
				],
    	]
		],
	];
 	//> SEO

	//breadcrumb
	$options[] = [
		'type'    => 'box',
		'title'   => __('Дополнительные настройки', 'fw2'),
		'attr'    => ['class' => 'custom-class', 'data-foo' => 'bar'],
		'options' => [
			'custom_breadcrumb'  => [
				'type' => 'text',
				'label' => 'Хлебные крошки',
				'max' => 2,
				'fw-storage' => [
					'type' => 'post-meta',
					'post-meta' => 'custom_breadcrumb',
				],
			],
			 // Фильтр для объектов вторичной недвижимости
			'filter_estate'  => [
				'type' => 'text',
				'label' => 'Массив объектов строкой',
				'max' => 2,
				'fw-storage' => [
					'type' => 'post-meta',
					'post-meta' => 'filter_settings',
				],
			]
		]
	];


	//< SETTINGS TEMPLATE
	if ($page_template_slug === 'templates/template_lp1.php') {
		$options[] = [
			'type'    => 'box',
			'title'   => __('Настройки шаблона', 'fw3'),
			'attr'    => ['class' => 'custom-class', 'data-foo' => 'bar'],
			'options' => [

				'panel_bunner' => [
					'type'    => 'tab',
					'title'   => __('Баннер', 'fw3'),
					'options' => [
						'title_bunner'  => [
							'type' => 'text',
							'label' => 'Заголовок баннера',
							'max' => 2,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'title_bunner',
							],
						],
						'subtitle_bunner'  => [
							'type' => 'text',
							'label' => 'Подзаголовок баннера',
							'max' => 2,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'subtitle_bunner',
							],
						],
						'btn_title_bunner'  => [
							'type' => 'text',
							'label' => 'Название кнопки',
							'max' => 2,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'btn_title_bunner',
							],
						],
						'panel_bunner_bg' => [
							'type'  => 'upload',
							'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
							'label' => __('Фон', '{domain}'),
							'images_only' => true,
							'files_ext' => array( 'webp', 'jpeg',  'jpg',  'png',  'svg'),
							'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'panel_bunner_bg',
							],
						],
					]
				],

				'panel_sldier' => [
					'type'    => 'tab',
					'title'   => __('Слайдер', 'fw3'),
					'options' => [
						'panel_slider_bg' => [
							'type'  => 'multi-upload',
							'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
							'label' => __('Изображения', '{domain}'),
							'images_only' => true,
							'files_ext' => array( 'webp', 'jpeg',  'jpg',  'png',  'svg'),
							'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'panel_slider_bg',
							],
						],
					]
				],

				'panel_advantage' => [
					'type'    => 'tab',
					'title'   => __('Преимущества', 'fw3'),
					'options' => [
						'panel_advantage_block_title'  => [
							'type' => 'text',
							'label' => 'Заголовок блока',
							'max' => 2,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'panel_advantage_block_title',
							],
						],

						'panel_advantages' => array(
						    'type'  => 'addable-box',
						    'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
						    'label' => __('Преимущество', '{domain}'),
						    'box-options' => array(
									'panel_advantage_title'  => [
										'type' => 'text',
										'label' => 'Заголовок',
										'max' => 2,
									],
						      'panel_advantage_desc' => array(
										'type' => 'textarea', 'label' => 'Описание',
									),
									'panel_advantage_icon' => array(
									    'type'  => 'upload',
									    'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
									    'label' => __('Иконка', '{domain}'),
									    'images_only' => true,
									    'files_ext' => array( 'webp', 'jpeg',  'jpg',  'png',  'svg'),
									    'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
									)
						    ),
								'fw-storage' => [
									'type' => 'post-meta',
									'post-meta' => 'panel_advantages',
								],
						    'template' => '{{- panel_advantage_title }}', // box title

						    'limit' => 0, // limit the number of boxes that can be added
						    'add-button-text' => __('Добавить', '{domain}'),
						    'sortable' => true,
						)
					]
				],

				'panel_manager' => [
					'type'    => 'tab',
					'title'   => __('Менеджер', 'fw3'),
					'options' => [
						'panel_manager_title'  => [
							'type' => 'text',
							'label' => 'Заголовок блока',
							'max' => 2,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'panel_manager_title',
							],
						],
						'panel_manager_name'  => [
							'type' => 'text',
							'label' => 'Имя менеджера',
							'max' => 2,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'panel_manager_name',
							],
						],
						'panel_manager_phone'  => [
							'type' => 'text',
							'label' => 'Телефон менеджера',
							'desc' => 'Пример: 9220342567',
							'max' => 2,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'panel_manager_phone',
							],
						],
						'panel_manager_avatar' => array(
								'type'  => 'upload',
								'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
								'label' => __('Аватар', '{domain}'),
								'desc' => "размер изображения 180x180",
								'images_only' => true,
								'files_ext' => array( 'webp', 'jpeg',  'jpg',  'png',  'svg'),
								'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
								'fw-storage' => [
									'type' => 'post-meta',
									'post-meta' => 'panel_manager_avatar',
								],
						)
					]
				],

				'panel2_advantage' => [
					'type'    => 'tab',
					'title'   => __('Преимущества - 2', 'fw3'),
					'options' => [
						'panel2_advantage_block_title'  => [
							'type' => 'text',
							'label' => 'Заголовок блока',
							'max' => 2,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'panel2_advantage_block_title',
							],
						],
						'panel2_advantages' => array(
						    'type'  => 'addable-box',
						    'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
						    'label' => __('Преимущество', '{domain}'),
						    'box-options' => array(
									'panel2_advantage_title'  => [
										'type' => 'text',
										'label' => 'Заголовок',
										'max' => 2,
									],
						      'panel2_advantage_desc' => array(
										'type' => 'textarea', 'label' => 'Описание',
									),
									'panel2_advantage_icon' => array(
									    'type'  => 'upload',
									    'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
									    'label' => __('Иконка', '{domain}'),
									    'images_only' => true,
									    'files_ext' => array( 'webp', 'jpeg',  'jpg',  'png',  'svg'),
									    'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
									),
									'panel2_advantage_url' => array(
										'type' => 'text', 'label' => 'Ссылка',
									),
						    ),
								'fw-storage' => [
									'type' => 'post-meta',
									'post-meta' => 'panel2_advantages',
								],
						    'template' => '{{- panel2_advantage_title }}', // box title

						    'limit' => 0, // limit the number of boxes that can be added
						    'add-button-text' => __('Добавить', '{domain}'),
						    'sortable' => true,
						)
					]
				],

				'panel3_advantage' => [
					'type'    => 'tab',
					'title'   => __('Преимущества - 3', 'fw3'),
					'options' => [
						'panel3_advantage_block_title'  => [
							'type' => 'text',
							'label' => 'Заголовок блока',
							'max' => 2,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'panel3_advantage_block_title',
							],
						],
						'panel3_advantages_' => array(
						    'type'  => 'addable-box',
						    'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
						    'label' => __('Преимущество', '{domain}'),
						    'box-options' => array(
									'panel3_advantage_title'  => [
										'type' => 'text',
										'label' => 'Заголовок',
										'max' => 2,
									],
						      'panel3_advantage_desc' => array(
										'type' => 'textarea', 'label' => 'Описание',
									),
						    ),
								'fw-storage' => [
									'type' => 'post-meta',
									'post-meta' => 'panel3_advantages',
								],
						    'template' => '{{- panel3_advantage_title }}', // box title

						    'limit' => 0, // limit the number of boxes that can be added
						    'add-button-text' => __('Добавить', '{domain}'),
						    'sortable' => true,
						)
					]
				],


				'panel4_advantage' => [
					'type'    => 'tab',
					'title'   => __('Преимущества - 4', 'fw3'),
					'options' => [
						'panel4_advantage_block_title'  => [
							'type' => 'text',
							'label' => 'Заголовок',
							'max' => 2,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'panel4_advantage_block_title',
							],
						],
						'panel4_advantages' => array(
						    'type'  => 'addable-box',
						    'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
						    'label' => __('Преимущество', '{domain}'),
						    'box-options' => array(
									'panel4_advantage_title'  => [
										'type' => 'text',
										'label' => 'Заголовок',
										'max' => 2,
									],
						      'panel4_advantage_desc' => array(
										'type' => 'textarea', 'label' => 'Описание',
									),
									'panel4_advantage_icon' => array(
									    'type'  => 'upload',
									    'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
									    'label' => __('Иконка', '{domain}'),
									    'images_only' => true,
									    'files_ext' => array( 'webp', 'jpeg',  'jpg',  'png',  'svg'),
									    'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
									)
						    ),
								'fw-storage' => [
									'type' => 'post-meta',
									'post-meta' => 'panel4_advantages',
								],
						    'template' => '{{- panel4_advantage_title }}', // box title
						    'limit' => 0, // limit the number of boxes that can be added
						    'add-button-text' => __('Добавить', '{domain}'),
						    'sortable' => true,
						)
					]
				],

				'panel_form' => [
					'type'    => 'tab',
					'title'   => __('Форма', 'fw3'),
					'options' => [
						'panel_form_title'  => [
							'type' => 'text',
							'label' => 'Заголовок ',
							'max' => 2,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'panel_form_title',
							],
						],
						'panel_form_subtitle'  => [
							'type' => 'text',
							'label' => 'Подзаголовок',
							'max' => 2,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'panel_form_subtitle',
							],
						],
						'panel_form_btn_name'  => [
							'type' => 'text',
							'label' => 'Название кнопки',
							'max' => 2,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'panel_form_btn_name',
							],
						],
					]
				],

				'panel_news' => [
					'type'    => 'tab',
					'title'   => __('Ссылки блоками', 'fw3'),
					'options' => [
						'panel_news_ids'  => [
							'type' => 'text',
							'label' => 'ID страниц ',
							'max' => 2,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'panel_news_ids',
							],
						],
					]
				],

				'panel_faq' => [
					'type'    => 'tab',
					'title'   => __('Вопросы и ответы', 'fw3'),
					'options' => [
						'panel_faq_block_title'  => [
							'type' => 'text',
							'label' => 'Название блока',
							'value' => '',
							'help' => 'По умолчанию: Ответы на главные вопросы',
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'panel_faq_block_title',
							],
						],
						'panel_faq_questions' => array(
						    'type'  => 'addable-box',
						    'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
						    'label' => __('Вопрос', '{domain}'),
						    'box-options' => array(
									'panel_faq_questions_title' => [
										'type' => 'text',
										'label' => 'Вопрос',
										'max' => 2,
									],
						      'panel_faq_questions_desc' => array(
										'type' => 'textarea', 'label' => 'Ответ',

									),
						    ),
								'fw-storage' => [
									'type' => 'post-meta',
									'post-meta' => 'panel_faq_questions',
								],
						    'template' => '{{- panel_faq_questions_title }}', // box title
						    'limit' => 0, // limit the number of boxes that can be added
						    'add-button-text' => __('Добавить', '{domain}'),
						    'sortable' => true,
						)
					]
				],

			]
		];
	}

	if ($page_template_slug === 'templates/template_zhk.php') {
		$options[] = [
			'type'    => 'box',
			'title'   => __('Настройки шаблона', 'fw3'),
			'attr'    => ['class' => 'custom-class', 'data-foo' => 'bar'],
			'options' => [
				'crm_zhk_id' => [
					'type'  => 'text',
						'value' => '',
						'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
						'label' => __('CRM ID ЖК', '{domain}'),
						'fw-storage' => [
							'type' => 'post-meta',
							'post-meta' => 'crm_zhk_id',
						]
				],
				'coordinate_x' => [
					'type'  => 'text',
						'value' => '',
						'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
						'label' => __('Координат X', '{domain}'),
						'fw-storage' => [
							'type' => 'post-meta',
							'post-meta' => 'coordinate_x',
						]
				],
				'coordinate_y' => [
					'type'  => 'text',
						'value' => '',
						'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
						'label' => __('Координат Y', '{domain}'),
						'fw-storage' => [
							'type' => 'post-meta',
							'post-meta' => 'coordinate_y',
						]
				],
				'panel_gallery' => [
					'type'    => 'tab',
					'title'   => __('Галлерея', 'fw3'),
					'options' => [
						'panel_gallery_subtitle' => [
							'type'  => 'text',
								'value' => '',
								'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
								'label' => __('Заголовок галлерии', '{domain}'),
								'fw-storage' => [
									'type' => 'post-meta',
									'post-meta' => 'panel_gallery_subtitle',
								]
						],
						'panel_gallery_bg' => [
							'type'  => 'multi-upload',
							'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
							'label' => __('Галлерея', '{domain}'),
							'images_only' => true,
							'files_ext' => array('jpeg',  'jpg'),
							'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'panel_gallery_bg',
							]
						],
					]
				],

				'panel_desc' => [
					'type'    => 'tab',
					'title'   => __('Описание', 'fw3'),
					'options' => [
						'panel_desc_block_title' => [
							'type'  => 'text',
								'value' => 'Описание',
								'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
								'label' => __('Заголовок блока', '{domain}'),
								'fw-storage' => [
									'type' => 'post-meta',
									'post-meta' => 'panel_desc_block_title',
								]
						],
						'panel_desc_text' => [
							'type'  => 'textarea',
								'value' => '',
								'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
								'label' => __('Заголовок галлереи', '{domain}'),
								'fw-storage' => [
									'type' => 'post-meta',
									'post-meta' => 'panel_desc_text',
								]
						],
					]
				],

				'panel_news' => [
					'type'    => 'tab',
					'title'   => __('Преимущества', 'fw3'),
					'options' => [
						'panel_news_ids'  => [
							'type' => 'text',
							'label' => 'ID страниц ',
							'max' => 2,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'panel_news_ids',
							],
						],
					]
				],
				'panel_plans' => [
					'type'    => 'tab',
					'title'   => __('Планировки', 'fw3'),
					'options' => [
						'panel_plans_items' => array(
					    'type' => 'addable-popup',
					    'label' => __('Комментарии', '{domain}'),
					    'template' => '{{- comment_name }}',
					    'popup-title' => null,
					    'size' => 'small', // small, medium, large
					    'limit' => 0, // limit the number of popup`s that can be added
					    'add-button-text' => __('Добавить', '{domain}'),
					    'sortable' => true,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'panel_plans_items',
							],
					    'popup-options' => array(
								'panel_plans_flat' => array(
										'label' => __('Комнаты', '{domain}'),
										'type' => 'text',
										'value' => '',
								),
				        'panel_plans_flat_square' => array(
				            'label' => __('Площадь', '{domain}'),
				            'type' => 'text',
				            'value' => 'К',
				        ),
								'panel_plans_flat_price' => array(
				            'label' => __('Цена', '{domain}'),
				            'type' => 'text',
				            'value' => '',
				        ),
								'panel_plans_flat_ipoteka' => array(
				            'label' => __('В ипотеку', '{domain}'),
				            'type' => 'text',
				            'value' => '',
				        ),
				        'panel_plans_flat_gp' => array(
			            'label' => __('ГП', '{domain}'),
			            'type' => 'text',
			            'value' => '',
				        	),
								'panel_plans_flat_date' => array(
									'label' => __('Срок сдачи', '{domain}'),
									'type' => 'text',
									'value' => '',
									),
								'panel_plans_flat_images' => [
									'type'  => 'upload',
									'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
									'label' => __('Фон', '{domain}'),
									'images_only' => true,
									'files_ext' => array('jpeg',  'jpg',  'png'),
									'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
								],
					       ),
					    ),
					]
				],
				'panel_bunner' => [
					'type'    => 'tab',
					'title'   => __('Баннер', 'fw3'),
					'options' => [
						'title_bunner'  => [
							'type' => 'text',
							'label' => 'Заголовок баннера',
							'max' => 2,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'title_bunner',
							],
						],
						'subtitle_bunner'  => [
							'type' => 'text',
							'label' => 'Подзаголовок баннера',
							'max' => 2,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'subtitle_bunner',
							],
						],
						'btn_title_bunner'  => [
							'type' => 'text',
							'label' => 'Название кнопки',
							'max' => 2,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'btn_title_bunner',
							],
						],
						'panel_bunner_bg' => [
							'type'  => 'upload',
							'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
							'label' => __('Фон', '{domain}'),
							'images_only' => true,
							'files_ext' => array( 'webp', 'jpeg',  'jpg',  'png',  'svg'),
							'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'panel_bunner_bg',
							],
						],
					]
				],


				'sk' => [
					'type'    => 'tab',
					'title'   => __('Ход строительства', 'fw3'),
					'options' => [
						'sk_images' => [
							'type'  => 'multi-upload',
							'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
							'label' => __('Изображения', '{domain}'),
							'images_only' => true,
							'files_ext' => array( 'webp', 'jpeg',  'jpg',  'png',  'svg'),
							'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'sk_images',
							],
						],
					]
				],



				'panel_comments' => [
					'type'    => 'tab',
					'title'   => __('Отзывы', 'fw3'),
					'options' => [
						'comments' => array(
					    'type' => 'addable-popup',
					    'label' => __('Комментарии', '{domain}'),
					    'template' => '{{- comment_name }}',
					    'popup-title' => null,
					    'size' => 'small', // small, medium, large
					    'limit' => 0, // limit the number of popup`s that can be added
					    'add-button-text' => __('Добавить', '{domain}'),
					    'sortable' => true,
							'fw-storage' => [
								'type' => 'post-meta',
								'post-meta' => 'comments',
							],
					    'popup-options' => array(
									'comment_avatar' => array(
								    'type'  => 'upload',
								    'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
								    'label' => __('Аватар', '{domain}'),
								    'images_only' => true,
								    'files_ext' => array( 'png', 'jpg', 'jpeg' ),
								    'extra_mime_types' => array( 'audio/x-aiff, aif aiff' )
								),
					        'comment_name' => array(
					            'label' => __('Имя', '{domain}'),
					            'type' => 'text',
					            'value' => '',
					        ),
					        'comment_desc' => array(
					            'label' => __('Комментарий', '{domain}'),
					            'type' => 'textarea',
					            'value' => '',
					            ),
					        ),
					    ),
					]
				],

				'panel_faq' => [
					'type'    => 'tab',
					'title'   => __('FAQ', 'fw3'),
					'options' => [
					  'panel_faq_questions' => array(
					      'type'  => 'addable-box',
					      'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
					      'label' => __('Вопрос', '{domain}'),
					      'box-options' => array(
					        'panel_faq_questions_title' => [
					          'type' => 'text',
					          'label' => 'Вопрос',
					          'max' => 2,
					        ],
					        'panel_faq_questions_desc' => array(
					          'type' => 'textarea', 'label' => 'Ответ',

					        ),
					      ),
								'fw-storage' => [
									'type' => 'post-meta',
									'post-meta' => 'panel_faq_questions',
								],
					      'template' => '{{- panel_faq_questions_title }}', // box title
					      'limit' => 0, // limit the number of boxes that can be added
					      'add-button-text' => __('Добавить', '{domain}'),
					      'sortable' => true,
					  )
					]
				],

			]
		];
	}
	//> SETTINGS TEMPLATE
