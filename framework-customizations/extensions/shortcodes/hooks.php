<?php

if (!defined('FW')) die('Forbidden');

function _filter_theme_disable_default_shortcodes($to_disable) {
	$to_disable = [
		'accordion',
		'button',
		'calendar',
		'call_to_action',
		'divider',
		'icon',
		'icon_box',
		'map',
		'media_image',
		'media_video',
		'notification',
		'special_heading',
		'table',
		'tabs',
		'team_member',
		'testimonials',
		'text_block',
		'widget_area'
	];

    return $to_disable;
}
add_filter('fw_ext_shortcodes_disable_shortcodes', '_filter_theme_disable_default_shortcodes');