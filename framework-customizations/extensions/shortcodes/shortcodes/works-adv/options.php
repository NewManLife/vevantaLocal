<?php
$options = [
    'title'   => [
      'label'   => __('Заголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Новые вакансии в компании Веванта',
  ],
    'img'   => [
      'label'   => __('Картинка-фон формы', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
    'cards' => array(
        'type' => 'addable-popup',
        'label' => __('Карточка', '{domain}'),
        'popup-title' => null,
        'size' => 'small', // small, medium, large
        'template' => '{{- titleCard }}',
        'limit' => 0, // limit the number of popup`s that can be added
        'add-button-text' => __('Добавить', '{domain}'),
        'sortable' => true,
        'fw-storage' => [
        'type' => 'post-meta',
        'post-meta' => 'comments',
        ],
        'popup-options' => array(
                'titleCard' => array(
                    'label' => __('Заголовок карточки', '{domain}'),
                    'type' => 'text',
                    'value' => 'Специалист по продаже и аренде недвижимости',
                ),
                'subtitleCard' => array(
                    'label' => __('Подзаголовок карточки', '{domain}'),
                    'type' => 'text',
                    'value' => 'Зарплата от 50 000 руб. до вычета налогов (опыт работы: не требуется)',
                ),
                'firstTitleTextCard' => array(
                    'label' => __('Подзагловок текста карточки', '{domain}'),
                    'type' => 'text',
                    'value' => 'Специализация:',
                ),
                'cardFirstTexts' => array(
                    'type' => 'addable-popup',
                    'label' => __('Пункты подзаголовка', '{domain}'),
                    'popup-title' => null,
                    'size' => 'small', // small, medium, large
                    'template' => '{{- message }}',
                    'limit' => 0, // limit the number of popup`s that can be added
                    'add-button-text' => __('Добавить', '{domain}'),
                    'sortable' => true,
                    'fw-storage' => [
                    'type' => 'post-meta',
                    'post-meta' => 'comments',
                    ],
                    'popup-options' => array(
                            'message' => array(
                                'label' => __('Пункт подзаголовка', '{domain}'),
                                'type' => 'text',
                                'value' => 'Оказание риелторской услуги входящему потоку клиентов',
                            ),
                        ),
                    ),
                'secondTitleTextCard' => array(
                    'label' => __('Подзагловок текста карточки', '{domain}'),
                    'type' => 'text',
                    'value' => 'Условия:',
                ),
                'cardSecondTexts' => array(
                    'type' => 'addable-popup',
                    'label' => __('Пункты подзаголовка', '{domain}'),
                    'popup-title' => null,
                    'size' => 'small', // small, medium, large
                    'template' => '{{- message }}',
                    'limit' => 0, // limit the number of popup`s that can be added
                    'add-button-text' => __('Добавить', '{domain}'),
                    'sortable' => true,
                    'fw-storage' => [
                    'type' => 'post-meta',
                    'post-meta' => 'comments',
                    ],
                    'popup-options' => array(
                            'message' => array(
                                'label' => __('Пункт подзаголовка', '{domain}'),
                                'type' => 'text',
                                'value' => 'Стабильный входящий поток от компании',
                            ),
                        ),
                    ),
                'thirdTitleTextCard' => array(
                    'label' => __('Подзагловок текста карточки', '{domain}'),
                    'type' => 'text',
                    'value' => 'Ключевые навыки:',
                ),
                'cardThirdTexts' => array(
                    'type' => 'addable-popup',
                    'label' => __('Пункты подзаголовка', '{domain}'),
                    'popup-title' => null,
                    'size' => 'small', // small, medium, large
                    'template' => '{{- message }}',
                    'limit' => 0, // limit the number of popup`s that can be added
                    'add-button-text' => __('Добавить', '{domain}'),
                    'sortable' => true,
                    'fw-storage' => [
                    'type' => 'post-meta',
                    'post-meta' => 'comments',
                    ],
                    'popup-options' => array(
                            'message' => array(
                                'label' => __('Пункт подзаголовка', '{domain}'),
                                'type' => 'text',
                                'value' => 'Грамотная речь',
                            ),
                        ),
                    ),
                'buttonAdv' => array(
                    'label' => __('текст кнопки карточки', '{domain}'),
                    'type' => 'text',
                    'value' => 'оставить заявку',
                ),
                'targetButtonAdv' => array(
                    'label' => __('Таргет кнопки на модальное окна', '{domain}'),
                    'type' => 'text',
                    'value' => 'target: #modal-worksAdv',
                ),
                'urlButtonAdv' => array(
                    'label' => __('Url куда ведет кнопка', '{domain}'),
                    'type' => 'text',
                    'value' => '',
                ),
            ),
        ),

];
