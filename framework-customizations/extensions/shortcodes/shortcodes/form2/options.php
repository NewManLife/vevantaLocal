<?php

$options = [
  'img'   => [
    'type'  => 'upload',
    'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
    'label' => __('Изображение формы', '{domain}'),
    'images_only' => true,
    'files_ext' => array( 'webp', 'jpeg',  'jpg',  'png',  'svg'),
    'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
  ],
  'img-picker' => array(
    'type'  => 'multi-picker',
    'label' => false,
    'desc'  => false,
    'picker' => array(
      'content-type' => array (
        'label'   => __('Формат изображения', '{domain}'),
        'type'    => 'select', // or 'short-select'
        'choices' => array(
            'agent'  => __('Фото сотрудника', '{domain}'),
            'background' => __('Фоновое фото', '{domain}')
        ),
      ),
    ),
    'choices' => array(
      'agent' => array(
        'agent_name' => [
          'label'   => __('Имя сотрудника', '{domain}'),
          'type'    => 'text',
        ],
        'agent_position' => [
          'label'   => __('Должность', '{domain}'),
          'type'    => 'text',
        ],
      ),
      'background' => array(),
    ),
  ),
  'title'   => [
      'label'   => __('Заголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Консультируем бесплатно',
  ],
  'subtitle'   => [
    'label'   => __('Подзаголовок формы', '{domain}'),
    'type'    => 'text',
    'value' => 'Проведем для вас бесплатную экскурсию',
  ],
  'form'   => [
    'label'   => __('Название формы', '{domain}'),
    'type'    => 'text',
    'value' => 'Записаться на просмотр',
  ],
  'button_name'   => [
      'label'   => __('Название кнопки', '{domain}'),
      'type'    => 'text',
      'value' => 'Записаться на просмотр',
  ],
  'class_id'   => [
      'label'   => __('Class объекта(Тип недвижимости)', '{domain}'),
      'type'    => 'text',
      'help' => '72 по умолчанию Загородная, 74 - Квартира',
      'value' => 72,
  ],
  'staff_id'   => [
      'label'   => __('ID ответственного', '{domain}'),
      'type'    => 'text',
      'help' => '78 по умолчанию Лариса Кокорина',
      'value' => 78,
  ],
  'manager_id'   => [
      'label'   => __('ID менеджера', '{domain}'),
      'type'    => 'text',
      'help' => '0 - Отключен',
      'value' => 0,
  ], 
  'source_id'   => [
    'label'   => __('ID источника', '{domain}'),
    'type'    => 'text',
    'help' => '0 - Отключен',
    'value' => 5,
  ], 
];