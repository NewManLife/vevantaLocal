<?php

$options = [
  'title'   => [
      'label'   => __('Заголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Узнайте, сколько денег вы сможете взять в ипотеку?',
  ],

  'subtitle'   => [
      'label'   => __('Заголовок после карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Мы поможем вам получить самую выгодную ставку и одобрение банка!',
  ],

  'titleForm'   => [
      'label'   => __('Заголовок карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Позвоните нам по номеру и мы вам поможем:',
  ],

  'subtitleForm'   => [
      'label'   => __('текст после номера телефона в карточке', '{domain}'),
      'type'    => 'text',
      'value' => 'или оставьте нам заявку на бесплатную консультацию',
  ],

  'phoneNumber'   => [
      'label'   => __('Номер телефона указанный в карточке(при изменении указать новый номер для html)', '{domain}'),
      'type'    => 'text',
      'value' => '+7 (3452) 69-23-47',
  ],

  'telNumber'   => [
      'label'   => __('Номер телефона указанный в HTML, изменить при замене номера телефонга для карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'tel:+73452692347',
  ],

  'buttonText'   => [
      'label'   => __('Текст кнопки отправки формы', '{domain}'),
      'type'    => 'text',
      'value' => 'получить услугу',
  ],

  'img'   => [
      'label'   => __('Картинка-фон формы', '{domain}'),
      'type'    => 'text',
      'value' => 'https://vevanta.ru/wp-content/uploads/2020/10/%D0%B1%D0%B0%D0%BD%D0%B5%D1%80.png',
  ],

  'class_id'   => [
      'label'   => __('Class объекта(Тип недвижимости)', '{domain}'),
      'type'    => 'text',
      'value' => false,
  ],

  'staff_id'   => [
      'label'   => __('ID ответственного', '{domain}'),
      'type'    => 'text',
      'help' => '48 по умолчанию Марина Михно',
      'value' => 48,
  ],
  
  'manager_id'   => [
      'label'   => __('ID менеджера', '{domain}'),
      'type'    => 'text',
      'help' => '0 - Отключен',
      'value' => 0,
  ],  
  
];
