<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Ипотека верхний блок', 'fw' ),
	'description' => __( 'Компонент для отображения верхнего блока страницы ипотека', 'fw' ),
];
