<Ipoteka-Top
  title="<?=$atts['title']?>"

  subtitle="<?php echo $atts['subtitle']?>"
  
  title-form="<?php echo $atts['titleform']?>"

  subtitle-form="<?php echo $atts['subtitleform']?>"

  phone-number="<?php echo $atts['phonenumber']?>"

  tel-number="<?php echo $atts['telnumber']?>"

  button-text="<?php echo $atts['buttontext']?>"

  img="<?php echo $atts['img']?>"

  <?php if ($atts['staff_id']) {?>
    :staff-id="<?php echo $atts['staff_id'];?>"
  <?php }?>

  <?php if ($atts['manager_id'] || $atts['manager_id'] === '0') {?>
    :manager-id="<?php echo $atts['manager_id'];?>"
  <?php }?>

  <?php if ($atts['class_id']) {?>
    :class-id="<?php echo $atts['class_id'];?>"
  <?php }?>

></Ipoteka-Top>
