<?php
$options = [
   'title'   => [
      'label'   => __('Заголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Получите карту лояльности Веванта и множество скидок и эксклюзивных предложений от партнеров компании Веванта',
  ],
  'textAfter'   => [
      'label'   => __('Текст после отправки формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Ваше заявка отправлена',
  ],
  'imgFirstCard'   => [
      'label'   => __('Url картинки первой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'https://vevanta.ru/wp-content/themes/agentstvo/assets/images/club/advantage1.jpg',
  ],
  'firstCards'   => [
      'label'   => __('текст первой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Совершите сделку с компанией Веванта',
  ],
  'imgSecondCard'   => [
      'label'   => __('Url картинки второй карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'https://vevanta.ru/wp-content/themes/agentstvo/assets/images/club/advantage2.jpg',
  ],
  'secondCard'   => [
      'label'   => __('текст второй карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Получите карту лояльности',
  ],
  'imgThirdCard'   => [
      'label'   => __('Url картинки третьей карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'https://vevanta.ru/wp-content/themes/agentstvo/assets/images/club/advantage3.jpg',
  ],
  'thirdCard'   => [
      'label'   => __('текст третьей карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Обратитесь с картой к партнёрам и получите скидки',
  ],
  'buttonText'   => [
      'label'   => __('текст кнопки', '{domain}'),
      'type'    => 'text',
      'value' => 'Отправить',
  ],
  'staffId'   => [
      'label'   => __('staffId', '{domain}'),
      'type'    => 'text',
      'value' => '1320',
  ],
    'managerId'   => [
      'label'   => __('managerId', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
    'classId'   => [
      'label'   => __('classId', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
    'sourceId'   => [
      'label'   => __('sourceId', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
  
];
