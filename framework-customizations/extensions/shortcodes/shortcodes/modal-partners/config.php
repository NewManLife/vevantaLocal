<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Модальное окно для заявки на карту лояльности', 'fw' ),
	'description' => __( 'Модальное окно для отправки информации на карту лояльности', 'fw' ),
];
