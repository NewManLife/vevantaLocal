<?php
$options = [

  'bullet'   => [
      'label'   => __('Иконка для пункта списка', '{domain}'),
      'type'    => 'text',
      'value' => 'https://www/vevanta.ru/wp-content/uploads/2021/03/icon_check.png',
  ],


  'items' => array(
    'type'  => 'addable-box',
    'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
    'label' => __('Пункт списка', '{domain}'),
    'box-options' => array(
      'item_text'  => [
        'type' => 'text',
        'label' => 'Описание'
      ],
    ),
    'template' => '{{- item_text }}', // box title
    'add-button-text' => __('Добавить', '{domain}'),
    'sortable' => true,
  )
];

?>
