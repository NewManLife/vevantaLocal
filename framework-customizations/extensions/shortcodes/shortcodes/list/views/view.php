<?php
  $items = array();
  for($i = 0; $i < count($atts['items']); $i++) {
    array_push($items, $atts['items'][$i]['item_text']);
  }
?>
<Mortgage-List


  bullet="<?=$atts['bullet']?>"
  :listitem="<?php echo htmlspecialchars(json_encode($items), ENT_QUOTES, 'UTF-8'); ?>"

></Mortgage-List>
