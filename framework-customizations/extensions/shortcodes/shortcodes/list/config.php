<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Список с настраиваемой иконкой', 'fw' ),
	'description' => __( 'На больших экранах пункты идут в 2 колонки, на маленьких - в одну', 'fw' ),	
];
