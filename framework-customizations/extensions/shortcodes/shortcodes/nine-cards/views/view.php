<Nine-Cards

title="<?=$atts['title']?>"
img="<?php echo $atts['img']?>"
img-right="<?php echo $atts['imgright']?>"
img-width="<?php echo $atts['imgwidth']?>"

cards="<?php echo htmlspecialchars(json_encode($atts['cards']), ENT_QUOTES, 'UTF-8'); ?>"

></Nine-Cards>