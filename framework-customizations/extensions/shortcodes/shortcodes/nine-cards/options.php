<?php
$options = [
    'title'   => [
      'label'   => __('Заголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Новые возможности и бонусы при работе с компанией ВЕВАНТА',
  ],
    'img'   => [
      'label'   => __('Url Картинки фона формы', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
    'imgRight'   => [
      'label'   => __('Url Картинки справа от карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'https://i.ibb.co/4VHFtkV/6550661.png',
  ],
  'imgWidth'   => [
      'label'   => __('Размер Картинки справа от карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'max-width: 700px;',
  ],
    'cards' => array(
        'type' => 'addable-popup',
        'label' => __('Карточка', '{domain}'),
        'popup-title' => null,
        'size' => 'small', // small, medium, large
        'template' => '{{- buttonText }}',
        'limit' => 0, // limit the number of popup`s that can be added
        'add-button-text' => __('Добавить', '{domain}'),
        'sortable' => true,
        'fw-storage' => [
        'type' => 'post-meta',
        'post-meta' => 'comments',
        ],
        'popup-options' => array(
                'textCardFirst' => array(
                    'label' => __('Первый текст карточки', '{domain}'),
                    'type' => 'text',
                    'value' => 'Бесплатная консультация по рынку недвижимости',
                ),
                'imgCardFirst' => array(
                    'label' => __('Url первой картинки', '{domain}'),
                    'type' => 'text',
                    'value' => 'https://freepikpsd.com/wp-content/uploads/2019/10/consultation-png-Transparent-Images.png',
                ),
                'textCardSecond' => array(
                    'label' => __('Второй текст карточки', '{domain}'),
                    'type' => 'text',
                    'value' => 'Сопровождение сделки юристами, оформление в МФЦ и банке',
                ),
                'imgCardSecond' => array(
                    'label' => __('Url второй картинки', '{domain}'),
                    'type' => 'text',
                    'value' => 'https://icon-library.com/images/consultation-icon/consultation-icon-4.jpg',
                ),
                'textCardThird' => array(
                    'label' => __('Третий текст карточки', '{domain}'),
                    'type' => 'text',
                    'value' => 'Помощь при торге и переговоры с владельцами недвижимости',
                ),
                'imgCardThird' => array(
                    'label' => __('Url третьей картинки', '{domain}'),
                    'type' => 'text',
                    'value' => 'https://thumbs.dreamstime.com/b/house-tree-logo-real-estate-image-vector-design-graphic-symbol-template-105126143.jpg',
                ),
                'textCardFourth' => array(
                    'label' => __('Четвертый текст карточки', '{domain}'),
                    'type' => 'text',
                    'value' => 'Просмотр недвижимости в удобное для вас время',
                ),
                'imgCardFourth' => array(
                    'label' => __('Url четвертой картинки', '{domain}'),
                    'type' => 'text',
                    'value' => 'https://png.pngtree.com/png-vector/20200116/ourmid/pngtree-man-with-happy-face-concept-for-expression-character-flat-style-vector-png-image_2129312.jpg',
                ),
                'buttonText' => array(
                    'label' => __('Текст кнопки', '{domain}'),
                    'type' => 'text',
                    'value' => 'получить бесплатную консультацию',
                ),
                'targetCard' => array(
                    'label' => __('Таргет карточки на модальное окна', '{domain}'),
                    'type' => 'text',
                    'value' => 'target: #modal-partners',
                ),
                'urlCard' => array(
                    'label' => __('Url куда ведет карточка', '{domain}'),
                    'type' => 'text',
                    'value' => '',
                ),
            ),
        ),

];
