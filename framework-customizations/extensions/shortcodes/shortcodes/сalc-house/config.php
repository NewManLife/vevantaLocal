<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Форма расчета стоимости дома checkbox три ряда', 'fw' ),
	'description' => __( 'Компонент форма расчета стоимости дома checkbox в три ряда', 'fw' ),
];
