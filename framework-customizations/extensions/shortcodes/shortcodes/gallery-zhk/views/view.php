<section class="uk-container">
  <div class="uk-margin-xlarge-top uk-hidden@m"></div>
  <h1><?php echo $atts['title']; ?></h1>
  <div class="uk-grid">
    <div class="uk-width-expand@l">
      <div class="object__gallery uk-margin-bottom">
        <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow>
            <ul class="uk-slideshow-items" uk-lightbox="animation: slide">
              <?php foreach ($atts['panel_gallery_bg'] as $item): ?>
                <li>
                  <a href="<?php echo $item['url'];?>">
                    <div
                      data-src="<?php echo wp_get_attachment_image_src($item['attachment_id'], 'medium')[0];?>"
                      uk-img
                      class="blur20 uk-position-absolute uk-background-cover uk-position-top-left uk-height-1-1 uk-width-1-1 uk-background-blend-overlay"
                      >
                    </div>
                    <div class="uk-position-center uk-icon" uk-spinner>

                    </div>
                    <div data-src="<?php echo wp_get_attachment_image_src($item['attachment_id'], 'small')[0];?>" uk-img
                      class="gallery__bg uk-position-absolute uk-height-1-1 uk-width-1-1 uk-background-contain">
                    </div>
                  </a>
                </li>
              <?php endforeach; ?>


            </ul>
            <div class="slider__route">
              <a class="uk-position-center-left" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
              <a class="uk-position-center-right" href="#"  uk-slidenav-next uk-slideshow-item="next"></a>
            </div>
        </div>
      </div>
    </div>
    <div class="uk-visible@l">
      <div class="uk-text-center">
        <img data-src="<?php echo get_template_directory_uri();?>/assets/images/header/logo-m.png" alt="" uk-img>
      </div>
      <hr>
      <div>
        <div class="uk-position-relative uk-width-auto">
          <img data-src="http://static.maps.2gis.com/1.0?center=<?php echo $atts['coordinate_x'];?>,<?php echo $atts['coordinate_y'];?>&zoom=15&size=295,250" alt="" uk-img>
          <img class="uk-position-center" data-src="<?php echo get_template_directory_uri();?>/assets/images/icons/placemark.svg" alt="" uk-img>
        </div>
      </div>
      <div class="uk-text-center uk-margin-top">
        <img data-src="https://vevanta.ru/wp-content/themes/agentstvo/assets/images/header/phone.png" alt="" uk-img>
        <a href="tel:+73452692347" class="fs18 cl-black fnt-med">+7 (3452) 69-23-47</a>
      </div>
    </div>
  </div>

  <div class="fs24@l fs20m fs18 fnt-med uk-margin-small-top uk-margin-medium-bottom">
      <?php echo $panel_gallery_subtitle; ?>
  </div>
</section>
