<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Слайдер ЖК', 'fw' ),
	'description' => __( 'Слайдер ЖК', 'fw' ),
	'tab'         => __( 'Формы', 'forms' ),
];
