<?php

$options = [
  'title' => [
    'type'  => 'text',
      'value' => '',
      'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
      'label' => __('Заголовок', '{domain}'),
      'fw-storage' => [
        'type' => 'post-meta',
        'post-meta' => 'coordinate_x',
      ]
  ],
  'coordinate_x' => [
    'type'  => 'text',
      'value' => '',
      'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
      'label' => __('Координат X', '{domain}'),
      'fw-storage' => [
        'type' => 'post-meta',
        'post-meta' => 'coordinate_x',
      ]
  ],
  'coordinate_y' => [
    'type'  => 'text',
      'value' => '',
      'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
      'label' => __('Координат Y', '{domain}'),
      'fw-storage' => [
        'type' => 'post-meta',
        'post-meta' => 'coordinate_y',
      ]
  ],
  'panel_gallery_subtitle' => [
    'type'  => 'text',
      'value' => '',
      'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
      'label' => __('Заголовок галлереи', '{domain}'),
  ],
  'panel_gallery_bg' => [
    'type'  => 'multi-upload',
    'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
    'label' => __('Галлерея', '{domain}'),
    'images_only' => true,
    'files_ext' => array('jpeg',  'jpg'),
    'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
  ],
];
