<?php

$options = [
  'village_id'   => [
      'label'   => __('ID developer', '{domain}'),
      'type'    => 'text',
      'popup_size' => 'small'
  ],
];
