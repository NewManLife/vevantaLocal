<?php if ( ! defined( 'FW' ) ) die( 'Forbidden' );

$cfg = [];
$cfg['page_builder'] = [
	'title'       => __( 'Карта участков', 'unyson' ),
	'description' => __( 'Карта участков', 'unyson' ),
	'tab'         => __( 'Content Elements', '{domain}'),
];
