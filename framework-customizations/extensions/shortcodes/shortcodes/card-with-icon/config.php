<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Карточки - настраиваемые', 'fw' ),
	'description' => __( 'Компонент для отображения карточек с иконкой + заголовком и текстом', 'fw' ),
];
