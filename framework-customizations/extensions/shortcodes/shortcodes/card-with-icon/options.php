<?php
$options = [
    'title'   => [
      'label'   => __('Заголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Работа в центре недвижимости Веванта',
  ],
  'width'   => [
      'label'   => __('Количество карточек в одной строке', '{domain}'),
      'type'    => 'text',
      'value' => 'uk-child-width-1-3@s',
  ],
    'img'   => [
      'label'   => __('Картинка-фон формы', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
    'cards' => array(
        'type' => 'addable-popup',
        'label' => __('Карточка', '{domain}'),
        'popup-title' => null,
        'size' => 'small', // small, medium, large
        'template' => '{{- titleCard }}',
        'limit' => 0, // limit the number of popup`s that can be added
        'add-button-text' => __('Добавить', '{domain}'),
        'sortable' => true,
        'fw-storage' => [
        'type' => 'post-meta',
        'post-meta' => 'comments',
        ],
        'popup-options' => array(
                'titleCard' => array(
                    'label' => __('Заголовок карточки', '{domain}'),
                    'type' => 'text',
                    'value' => 'Условия',
                ),
                'styleCard' => array(
                    'label' => __('Стиль заголовка карточки', '{domain}'),
                    'type' => 'text',
                    'value' => '',
                ),
                'imgCard' => array(
                    'label' => __('Url иконки карточки 60х60', '{domain}'),
                    'type' => 'text',
                    'value' => 'https://vevanta.ru/wp-content/uploads/2020/11/4-%D0%98%D0%B7%D1%83%D1%87%D0%B5%D0%BD%D0%B8%D0%B5-%D1%8E%D1%80%D0%B8%D1%81%D1%82%D0%B0%D0%BC%D0%B8-%D0%B4%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%BE%D0%B2.png',
                ),
                'textCard' => array(
                    'type' => 'addable-popup',
                    'label' => __('Пункты карточки', '{domain}'),
                    'popup-title' => null,
                    'size' => 'small', // small, medium, large
                    'template' => '{{- message }}',
                    'limit' => 0, // limit the number of popup`s that can be added
                    'add-button-text' => __('Добавить', '{domain}'),
                    'sortable' => true,
                    'fw-storage' => [
                    'type' => 'post-meta',
                    'post-meta' => 'comments',
                    ],
                    'popup-options' => array(
                            'message' => array(
                                'label' => __('Пункт карточки', '{domain}'),
                                'type' => 'text',
                                'value' => '- До 51% комиссия риелтора от сделки',
                            ),
                        ),
                    ),
            ),
        ),
    'classTitle'   => [
      'label'   => __('Класс заголовка формы', '{domain}'),
      'type'    => 'text',
      'value' => 'uk-card-title fs18 fs20@s fs24@m fs28@l fnt-bld',
  ],
    'classTitleCard'   => [
      'label'   => __('Класс заголовка карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'fs18 fs20@s fs24@l fnt-bld',
  ],
    'classTextCard'   => [
      'label'   => __('Класс текста карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'fs16 fs18@l uk-margin-top uk-text-left',
  ],

];
