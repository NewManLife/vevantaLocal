<Card-With-Icon

title="<?=$atts['title']?>"
width="<?=$atts['width']?>"
img="<?php echo $atts['img']?>"
class-title="<?php echo $atts['classtitle']?>"
class-title-card="<?php echo $atts['classtitlecard']?>"
class-text-card="<?php echo $atts['classtextcard']?>"

cards="<?php echo htmlspecialchars(json_encode($atts['cards']), ENT_QUOTES, 'UTF-8'); ?>"

></Card-With-Icon>