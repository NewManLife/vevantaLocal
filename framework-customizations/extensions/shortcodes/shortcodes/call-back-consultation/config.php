<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Форма обратного звонка', 'fw' ),
	'description' => __( 'Компонент для страницы налогового вычета', 'fw' ),
];
