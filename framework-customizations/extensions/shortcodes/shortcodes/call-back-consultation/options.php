<?php

$options = [
  'title'   => [
      'label'   => __('Заголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Узнайте сумму вашего налогового вычета',
  ],

  'subtitle'   => [
      'label'   => __('Подзаголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Оставьте заявку и мы вам перезвоним или звоните нам по телефону:',
  ],

  'phoneNumber'   => [
      'label'   => __('Номер телефона указанный в карточке', '{domain}'),
      'type'    => 'text',
      'value' => '+7 (3452) 69-23-47',
  ],

  'telNumber'   => [
      'label'   => __('Номер телефона указанный в HTML, изменить при замене номера телефона', '{domain}'),
      'type'    => 'text',
      'value' => 'tel:+73452692347',
  ],

  'buttonText'   => [
      'label'   => __('Текст кнопки отправки формы', '{domain}'),
      'type'    => 'text',
      'value' => 'узнать сумму',
  ],

  'img'   => [
      'label'   => __('Картинка-фон формы', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],

  'class_id'   => [
      'label'   => __('Class объекта(Тип недвижимости)', '{domain}'),
      'type'    => 'text',
      'value' => false,
  ],

  'staff_id'   => [
      'label'   => __('ID ответственного', '{domain}'),
      'type'    => 'text',
      'help' => '48 по умолчанию Марина Михно',
      'value' => 48,
  ],
  
  'manager_id'   => [
      'label'   => __('ID менеджера', '{domain}'),
      'type'    => 'text',
      'help' => '0 - Отключен',
      'value' => 0,
  ],  
  
];
