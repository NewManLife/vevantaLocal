<?php
$options = [
  'panel2_advantage_block_title'  => [
    'type' => 'text',
    'label' => 'Заголовок блока',
    'max' => 2,
  ],
  'panel2_advantages' => array(
      'type'  => 'addable-box',
      'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
      'label' => __('Преимущество', '{domain}'),
      'box-options' => array(
        'panel2_advantage_title'  => [
          'type' => 'text',
          'label' => 'Заголовок',
          'max' => 2,
        ],
        'panel2_advantage_desc' => array(
          'type' => 'textarea', 'label' => 'Описание',
        ),
        'panel2_advantage_icon' => array(
            'type'  => 'upload',
            'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
            'label' => __('Иконка', '{domain}'),
            'images_only' => true,
            'files_ext' => array( 'webp', 'jpeg',  'jpg',  'png',  'svg'),
            'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
        ),
        'panel2_advantage_url' => array(
          'type' => 'text', 'label' => 'Ссылка',
        ),
      ),
      'template' => '{{- panel2_advantage_title }}', // box title

      'limit' => 0, // limit the number of boxes that can be added
      'add-button-text' => __('Добавить', '{domain}'),
      'sortable' => true,
  )
];
