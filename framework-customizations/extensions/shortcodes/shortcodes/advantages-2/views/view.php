<section class="uk-container uk-margin-medium-top">
  <?php if ($atts['panel2_advantage_block_title']) {?>
  <div class="title">
    <?php echo $atts['panel2_advantage_block_title'];?>
  </div>
  <?php }?>

  <div class="uk-grid uk-child-width-1-3@s">
    <?php foreach ($atts['panel2_advantages'] as $item) {?>
    <a class="uk-text-center uk-margin-bottom uk-padding-small shadow-v"  href="<?php echo $item['panel2_advantage_url'];?>" target="_blank">
      <div class="uk-width-1-4 uk-width-1-1@m uk-margin-auto">
        <img data-src="<?php echo $item['panel2_advantage_icon']['url'];?>" alt="" uk-img>
      </div>
      <div class="uk-margin-small-top fnt-bld fs18@m cl-dark uk-margin-small-bottom">
        <?php echo $item['panel2_advantage_title'];?>
      </div>
      <div class="cl-dark">
        <?php echo $item['panel2_advantage_desc'];?>
      </div>
      <?php if ($item['panel2_advantage_url']) {?>
        <div class="uk-margin-small-top">
          <div class="btn btn__tpl1 btn__green">Подробнее</div>
        </div>
      <?php } ?>
    </a>
    <?php }?>
  </div>
</section>
