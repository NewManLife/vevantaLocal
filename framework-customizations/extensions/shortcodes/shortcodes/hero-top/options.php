<?php

$options = [
  'title'   => [
      'label'   => __('Заголовок', '{domain}'),
      'type'    => 'text',
      'value' => 'Присоединяйся к команде профессионалов ЦН Веванта!',
  ],

  'subtitle'   => [
      'label'   => __('Подзаголовок', '{domain}'),
      'type'    => 'text',
      'value' => 'Приглашаем как опытных риелторов, так и новичков в сфере недвижимости',
  ],

  'img'   => [
      'label'   => __('Картинка-фон формы', '{domain}'),
      'type'    => 'text',
      'value' => 'https://vevanta.ru/wp-content/uploads/2020/10/%D0%98%D0%BF%D0%BE%D1%82%D0%B5%D0%BA%D0%B0-%D0%B2-%D0%A2%D1%8E%D0%BC%D0%B5%D0%BD%D0%B8-1024x576.jpg',
  ],

  'classTitle'   => [
      'label'   => __('Класс стиля заголовка', '{domain}'),
      'type'    => 'text',
      'value' => 'fs24 fs30@s fs40@m fs46@l',
  ],

  'classSubtitle'   => [
      'label'   => __('Класс стиля подзаголовка', '{domain}'),
      'type'    => 'text',
      'value' => 'fs20 fs24@s fs24@m fs24@l uk-margin-top',
  ],

];
