<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( '5 Карточек', 'fw' ),
	'description' => __( 'Компонент для отображения карточек', 'fw' ),
];
