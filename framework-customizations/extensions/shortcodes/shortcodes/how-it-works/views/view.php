<How-It-Works
  title="<?=$atts['title']?>"
  subtitle-first="<?=$atts['subtitlefirst']?>"
  subtitle-second="<?=$atts['subtitlesecond']?>"
  subtitle-third="<?=$atts['subtitlethird']?>"
  subtitle-fourth="<?=$atts['subtitlefourth']?>"
  subtitle-fiveth="<?=$atts['subtitlefiveth']?>"
  img="<?php echo $atts['img']?>"
  button-text="<?=$atts['buttontext']?>"
  url-button="<?php echo $atts['urlbutton']?>"
></How-It-Works>

