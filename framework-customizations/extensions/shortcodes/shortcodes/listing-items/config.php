<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Вывод квартир из базы', 'fw' ),
	'description' => __( 'Компонент выводит квартиры из базы карточками + ссылка при нажатии', 'fw' ),
];
