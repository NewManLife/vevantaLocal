<section id="map">
  <?php if ($atts['map_title']) { ?>
    <div class="uk-container">
      <div class="title uk-text-center">
        <?php echo $atts['map_title']; ?> на карте
      </div>
    </div>
  <?php }?>
  <div id="map-gis" class="uk-margin-top uk-width-1-1 uk-height-large">
  </div>
</section>
<script>
  let map;
  let mapDistricts = [];
  let mapzhkGroups = [];
  let markersCluster;

  mapInit = () => {
    map = null;
    mapDistricts = [];
    mapzhkGroups = [];
    markersCluster = null;

    DG.then(() => {
      return DG.plugin('https://2gis.github.io/mapsapi/vendors/Leaflet.markerCluster/leaflet.markercluster-src.js');
    })
    .then(() => {
      let scrollWheelZoom = false;

      let content = '<img src="<?php echo $atts['map_img']['url'];?>">';
      if(window.innerWidth <= 600){
        scrollWheelZoom = false;
      }
      map = DG.map('map-gis', {
        center: [<?php echo $atts['coordinate_y'];?>, <?php echo $atts['coordinate_x'];?>],
        zoom: 16,
        scrollWheelZoom: scrollWheelZoom
      });
      let zhkIcon = DG.icon({
        iconUrl: '/wp-content/themes/agentstvo/assets/images/icons/placemark.svg',
        iconRetinaUrl: '/wp-content/themes/agentstvo/assets/images/icons/placemark.svg',
        iconSize: [26, 36],
        iconAnchor: [13, 36],
        popupAnchor: [-1, 7],
      });

      let zhkPopup = DG.popup({
        maxWidth: 150,
        minWidth: 150,
        maxHeight: 300,
        sprawling: true,
        closeButton: false,
        autoClose: true,
        className: "map-zhk",
      })
      .setContent(content);
        DG.marker([<?php echo $atts['coordinate_y'];?>,<?php echo $atts['coordinate_x'];?>], {icon: zhkIcon})
        .addTo(map)
        .bindPopup(zhkPopup)
        .openPopup();

        if(markersCluster){
          markersCluster.addTo(map);
        }
    });
  }
  mapInit();
</script>
