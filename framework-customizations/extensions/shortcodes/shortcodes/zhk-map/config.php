<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Карта ЖК', 'fw' ),
	'description' => __( 'Карта ЖК', 'fw' ),
	'tab'         => __( 'Карта', 'forms' ),
];
