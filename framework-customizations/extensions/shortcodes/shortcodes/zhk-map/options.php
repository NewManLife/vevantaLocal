<?php

$options = [
  'map_title' => [
    'type'  => 'text',
      'value' => '',
      'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
      'label' => __('Заголовок', '{domain}'),
  ],
  'coordinate_x' => [
    'type'  => 'text',
      'value' => '',
      'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
      'label' => __('Координат X', '{domain}'),
  ],
  'coordinate_y' => [
    'type'  => 'text',
      'value' => '',
      'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
      'label' => __('Координат Y', '{domain}'),
  ],
  'map_img' => [
    'type'  => 'upload',
    'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
    'label' => __('Галлерея', '{domain}'),
    'images_only' => true,
    'files_ext' => array('jpeg',  'jpg'),
    'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
  ],
];
