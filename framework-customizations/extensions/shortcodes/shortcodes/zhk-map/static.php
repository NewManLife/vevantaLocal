<?php if (!defined('FW')) die('Forbidden');

// find the uri to the shortcode folder
$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/zhk-map');
wp_enqueue_style(
    'fw-shortcode-zhk-map',
    $uri . '/static/css/style.css'
);

wp_enqueue_script(
    'fw-shortcode-2gis',
    'https://maps.api.2gis.ru/2.0/loader.js?pkg=full'
);
