<?php
$options = [
    'title'   => [
      'label'   => __('Заголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Новые подарки и предложения от партнеров компании Веванта',
  ],
    'img'   => [
      'label'   => __('Url Картинки фона формы', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
    'cards' => array(
        'type' => 'addable-popup',
        'label' => __('Карточка', '{domain}'),
        'popup-title' => null,
        'size' => 'small', // small, medium, large
        'template' => '{{- titleCard }}',
        'limit' => 0, // limit the number of popup`s that can be added
        'add-button-text' => __('Добавить', '{domain}'),
        'sortable' => true,
        'fw-storage' => [
        'type' => 'post-meta',
        'post-meta' => 'comments',
        ],
        'popup-options' => array(
                'titleCard' => array(
                    'label' => __('Заголовок карточки', '{domain}'),
                    'type' => 'text',
                    'value' => 'Тюменский центр загородного строительство',
                ),
                'subtitleCard' => array(
                    'label' => __('Подзаголовок карточки', '{domain}'),
                    'type' => 'text',
                    'value' => 'Строительство и покупка готового дома',
                ),
                'buttonCard' => array(
                    'label' => __('текст кнопки', '{domain}'),
                    'type' => 'text',
                    'value' => 'Скидка',
                ),
                'imgCard' => array(
                    'label' => __('Url Картинки карточки', '{domain}'),
                    'type' => 'text',
                    'value' => 'https://vevanta.ru/wp-content/themes/agentstvo/assets/images/club/logo1.jpg',
                ),
                'targetCard' => array(
                    'label' => __('Таргет карточки на модальное окна', '{domain}'),
                    'type' => 'text',
                    'value' => 'target: #modal-partners',
                ),
                'urlCard' => array(
                    'label' => __('Url куда ведет карточка', '{domain}'),
                    'type' => 'text',
                    'value' => '',
                ),
            ),
        ),

];
