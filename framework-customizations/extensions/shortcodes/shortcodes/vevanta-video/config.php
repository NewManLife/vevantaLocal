<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Баннер Веванта', 'fw' ),
	'description' => __( 'Компонент для отбражения банера с текстом поверх видео', 'fw' ),
];
