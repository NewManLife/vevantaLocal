<?php

$options = [
    'urlVideo'   => [
      'label'   => __('Url video через iframe', '{domain}'),
      'type'    => 'text',
      'value' => 'https://www.youtube.com/embed/02pvQofTzNI',
  ],
    'urlVideoTeg'   => [
      'label'   => __('Teg от Url video для loop повторения видео', '{domain}'),
      'type'    => 'text',
      'value' => '02pvQofTzNI',
  ],
    'text'   => [
      'label'   => __('Текст поверх видео', '{domain}'),
      'type'    => 'text',
      'value' => 'Веванта',
  ],
    'testSize'   => [
      'label'   => __('Размер текста поверх видео в "vw"', '{domain}'),
      'type'    => 'text',
      'value' => '20vw',
    
  ],
];
