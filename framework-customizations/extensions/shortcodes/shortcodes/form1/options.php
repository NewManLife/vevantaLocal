<?php

$options = [
  'title'   => [
      'label'   => __('Заголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Консультируем бесплатно',
  ],
  'caption'   => [
      'label'   => __('Подзаголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Оставьте заявку или звоните нам по телефону:',
  ],
  'class_id'   => [
      'label'   => __('Class объекта(Тип недвижимости)', '{domain}'),
      'type'    => 'text',
      'value' => false,
  ],
  'staff_id'   => [
      'label'   => __('ID ответственного', '{domain}'),
      'type'    => 'text',
      'help' => '48 по умолчанию Марина Михно',
      'value' => 48,
  ],
  'manager_id'   => [
      'label'   => __('ID менеджера', '{domain}'),
      'type'    => 'text',
      'help' => '0 - Отключен',
      'value' => 0,
  ],  
];
