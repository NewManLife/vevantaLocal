<?php

$options = [
  'panel_advantage_block_title'  => [
    'type' => 'text',
    'label' => 'Заголовок блока',
  ],

  'panel_advantages' => array(
      'type'  => 'addable-box',
      'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
      'label' => __('Преимущество', '{domain}'),
      'box-options' => array(
        'panel_advantage_title'  => [
          'type' => 'text',
          'label' => 'Заголовок',
        ],
        'panel_advantage_desc' => array(
          'type' => 'textarea', 'label' => 'Описание',
        ),
        'panel_advantage_icon' => array(
            'type'  => 'upload',
            'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
            'label' => __('Иконка', '{domain}'),
            'desc' => 'Размер изображения 52x52',
            'images_only' => true,
            'files_ext' => array( 'webp', 'jpeg',  'jpg',  'png',  'svg'),
            'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
        )
      ),
      'template' => '{{- panel_advantage_title }}', // box title

      'limit' => 0, // limit the number of boxes that can be added
      'add-button-text' => __('Добавить', '{domain}'),
      'sortable' => true,
  ),

  'panel_manager_title'  => [
    'type' => 'text',
    'label' => 'Заголовок блока менеджера',
  ],
  'panel_manager_name'  => [
    'type' => 'text',
    'label' => 'Имя менеджера',
  ],
  'panel_manager_phone'  => [
    'type' => 'text',
    'label' => 'Телефон менеджера',
    'desc' => 'Пример: 9220342567',
  ],
  'panel_manager_avatar' => array(
      'type'  => 'upload',
      'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
      'label' => __('Аватар', '{domain}'),
      'desc' => "размер изображения 180x180",
      'images_only' => true,
      'files_ext' => array( 'webp', 'jpeg',  'jpg',  'png',  'svg'),
      'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
  )
];
