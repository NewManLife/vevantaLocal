<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Преимущества - 1', 'fw' ),
	'description' => __( 'Преимущества - 1', 'fw' ),
	'tab'         => __( 'Преимущества', 'forms' ),
];
