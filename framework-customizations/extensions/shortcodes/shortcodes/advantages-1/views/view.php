<section class="uk-container">
  <div class="">
    <div class="uk-grid">
      <div class="uk-width-expand@l">
          <?php if ($atts['panel_advantage_block_title']) {?>
          <div class="title">
            <?php echo $atts['panel_advantage_block_title'];?>
          </div>
          <?php }?>
        <div>

          <?php foreach ($atts['panel_advantages'] as $item) {?>
            <div class="uk-grid uk-grid-collapse advantage-item uk-width-2-3@m">
              <div class="uk-border-circle advantage-item__block-icon uk-width-auto uk-position-relative uk-margin-small-right">
                <img class="uk-position-center" data-src="<?php echo $item['panel_advantage_icon']['url'];?>" alt="" uk-img>
              </div>
              <div class="uk-width-expand">
                <div class="fnt-bld fs16 fs18@">
                  <?php echo $item['panel_advantage_title'];?>
                </div>
                <div class="uk-margin-small-top fs14 fs16@m">
                  <?php echo $item['panel_advantage_desc'];?>
                </div>
              </div>
            </div>
          <?php }?>
        </div>
      </div>

      <div class="uk-width-1-3 uk-visible@l uk-margin-medium-top" id="offset">
        <div></div>
        <div class="uk-padding-small uk-background-muted sticky top0">
          <manager-item
            <?php if ($atts['panel_manager_title']) {?>
              title="<?php echo $atts['panel_manager_title'];?>"
            <?php }?>
            avatar="https:<?php echo $atts['panel_manager_avatar']['url'];?>"
            name="<?php echo $atts['panel_manager_name'];?>"
            phone="<?php echo $atts['panel_manager_phone'];?>"
            form="<?php echo $atts['panel_manager_name'];?>"
          ></manager-item>
        </div>
      </div>
    </div>
  </div>
</section>
