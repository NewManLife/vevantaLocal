<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Фильтр поиска квартир SearchFilterHome', 'fw' ),
	'description' => __( 'Компонент для поиска обьектов в базе квартир', 'fw' ),
];
