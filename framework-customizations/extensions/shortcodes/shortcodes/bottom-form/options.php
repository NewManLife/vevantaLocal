<?php
$options = [
    'title'   => [
      'label'   => __('Заголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Хотите зарабатывать вместе с нами?',
  ],

    'subtitle'   => [
      'label'   => __('Подзаголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Мы поможем вам найти идеальную вакансию, с учетом ваших способностей.',
  ],

    'popUpText'   => [
      'label'   => __('Сообщение после отправки формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Ваше сообщение отправлено',
  ],
    'img'   => [
      'label'   => __('Url Картинки фона формы', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
    'imgRight'   => [
      'label'   => __('Url Картинки справа от формы', '{domain}'),
      'type'    => 'text',
      'value' => 'https://cnet1.cbsistatic.com/img/2Uu-j9QWFbPTGqOFBIHEA3bFLMg=/1200x630/2020/05/12/47f08d3b-c5e7-4002-9128-1d5b6b9d778f/dell-xps-17.png',
  ],
    'buttonText'   => [
      'label'   => __('Текст кнопки', '{domain}'),
      'type'    => 'text',
      'value' => 'отправить',
  ],
  'staffId'   => [
      'label'   => __('staffId', '{domain}'),
      'type'    => 'text',
      'value' => '1320',
  ],
    'managerId'   => [
      'label'   => __('managerId', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
    'classId'   => [
      'label'   => __('classId', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
    'sourceId'   => [
      'label'   => __('sourceId', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
];
