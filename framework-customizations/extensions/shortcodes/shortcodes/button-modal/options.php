<?php

$options = [
  'parentClasses'   => [
      'label'   => __('CSS Класс родительского блока', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],

  'nameClasses'   => [
      'label'   => __('CSS Класс кнопки', '{domain}'),
      'type'    => 'text',
      'value' => 'btn btn__tpl1 btn__green fr uk-text-lowercase',
  ],
  'position' => [
    'type'  => 'radio',
    'value' => 'center',
    'attr'  => ['class' => 'custom-class', 'data-foo' => 'bar'],
    'label' => __('Расположение формы', '{domain}'),
    'choices' => [
        'left' => __('слево', '{domain}'),
        'right' => __('Справа', '{domain}'),
        'center' => __('По центру', '{domain}'),
    ],
    'inline' => true,
  ],
  'name'   => [
      'label'   => __('Текст кнопки', '{domain}'),
      'type'    => 'text',
      'value' => 'Заказать звонок',
  ],

  'class_id'   => [
      'label'   => __('Class объекта(Тип недвижимости)', '{domain}'),
      'type'    => 'text',
      'value' => false,
  ],

  'staff_id'   => [
      'label'   => __('ID ответственного', '{domain}'),
      'type'    => 'text',
      'help' => '48 по умолчанию Марина Михно',
      'value' => 48,
  ],
  
  'manager_id'   => [
      'label'   => __('ID менеджера', '{domain}'),
      'type'    => 'text',
      'help' => '0 - Отключен',
      'value' => 0,
  ],  
  
];
