<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Кнопка модального окна обратного звонка', 'fw' ),
	'description' => __( 'Компонент кнопка для модального окна', 'fw' ),
];
