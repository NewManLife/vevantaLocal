<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Номер телефона + кнопка обратного звонка', 'fw' ),
	'description' => __( 'Компонент для обратного звонка', 'fw' ),
];
