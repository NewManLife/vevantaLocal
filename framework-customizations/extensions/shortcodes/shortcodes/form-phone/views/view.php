<?php
  $position = '';
  $width = 'uk-width-1-1';

  if ($atts['position'] === 'left') {
    $width = 'uk-width-2-3@l';
    $position = 'uk-align-left';
  }

  if ($atts['position'] === 'right') {
    $width = 'uk-width-2-3@l';
    $position = 'uk-align-right';
  }
?>


<div class="<?php echo $width?> <?php echo $position;?>">
  <Form-Phone
    form="<?=$atts['form']?>"
    button-name="<?=$atts['buttonname']?>"
    
    <?php if ($atts['staff_id']) {?>
      :staff-id="<?php echo $atts['staff_id'];?>"
    <?php }?>
    <?php if ($atts['manager_id'] || $atts['manager_id'] === '0') {?>
      :manager-id="<?php echo $atts['manager_id'];?>"
    <?php }?>
    <?php if ($atts['class_id']) {?>
      :class-id="<?php echo $atts['class_id'];?>"
    <?php }?>

  ></Form-Phone>
</div>

