<?php

$options = [
    'form'   => [
      'label'   => __('Название формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Форма',
  ],
    'buttonName'   => [
      'label'   => __('Текст кнопки отправки формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Отправить',
  ],
    'position' => [
      'type'  => 'radio',
      'value' => 'center',
      'attr'  => ['class' => 'custom-class', 'data-foo' => 'bar'],
      'label' => __('Расположение формы', '{domain}'),
      'choices' => [
          'left' => __('слево', '{domain}'),
          'right' => __('Справа', '{domain}'),
          'center' => __('По центру', '{domain}'),
      ],
      'inline' => true,
    ],
    
    'class_id'   => [
        'label'   => __('Class объекта(Тип недвижимости)', '{domain}'),
        'type'    => 'text',
        'value' => false,
    ],

    'staff_id'   => [
        'label'   => __('ID ответственного', '{domain}'),
        'type'    => 'text',
        'help' => '48 по умолчанию Марина Михно',
        'value' => 48,
    ],
    
    'manager_id'   => [
        'label'   => __('ID менеджера', '{domain}'),
        'type'    => 'text',
        'help' => '0 - Отключен',
        'value' => 0,
    ],  
  
];
