<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Калькулятор ипотеки', 'fw' ),
	'description' => __( 'Калькулятор ипотеки', 'fw' ),
	'tab'         => __( 'Формы', 'forms' ),
];
