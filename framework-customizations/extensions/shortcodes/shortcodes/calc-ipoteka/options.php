<?php

$options = [
  'price'   => [
      'label'   => __('Стоимость', '{domain}'),
      'type'    => 'text',
      'value' => 1000000,
  ],
  'percent'   => [
      'label'   => __('Процентная ставка', '{domain}'),
      'type'    => 'text',
      'value' => 8,
  ],

  'position' => [
    'type'  => 'radio',
    'value' => 'center',
    'attr'  => ['class' => 'custom-class', 'data-foo' => 'bar'],
    'label' => __('Расположение формы', '{domain}'),
    'choices' => [
        'left' => __('слево', '{domain}'),
        'right' => __('Справа', '{domain}'),
        'center' => __('По центру', '{domain}'),
    ],
    'inline' => true,
  ],

  'title'   => [
      'label'   => __('Заголовок', '{domain}'),
      'type'    => 'text',
      'value' => 'Купить в ипотеку',
  ],
  'btn_name'   => [
      'label'   => __('Название кнопки', '{domain}'),
      'type'    => 'text',
      'value' => 'Индивидуальный расчет',
  ],
  'class_id'   => [
      'label'   => __('Class объекта(Тип недвижимости)', '{domain}'),
      'type'    => 'text',
      'value' => false,
  ],
  'staff_id'   => [
      'label'   => __('ID ответственного', '{domain}'),
      'type'    => 'text',
      'help' => '48 по умолчанию Марина Михно',
      'value' => 48,
  ],
  'manager_id'   => [
      'label'   => __('ID менеджера', '{domain}'),
      'type'    => 'text',
      'help' => '0 - Отключен',
      'value' => 0,
  ],
];
