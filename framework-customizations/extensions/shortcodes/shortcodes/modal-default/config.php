<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Модальное окно для связки с кнопкой', 'fw' ),
	'description' => __( 'Компонент модальное окно которое вызывается кнопкой', 'fw' ),
];
