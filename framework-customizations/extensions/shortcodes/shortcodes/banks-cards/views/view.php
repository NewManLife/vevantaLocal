<Banks-Cards

  title="<?=$atts['title']?>"
  subtitle="<?=$atts['subtitle']?>"
  button-text="<?=$atts['buttontext']?>"
  url-button="<?php echo $atts['urlbutton']?>"

  text-first-card="<?=$atts['textfirstcard']?>"
  subtext-first-card="<?=$atts['subtextfirstcard']?>"
  url-svg-first-card="<?=$atts['urlsvgfirstcard']?>"

  text-second-card="<?=$atts['textsecondcard']?>"
  subtext-second-card="<?=$atts['subtextsecondcard']?>"
  url-svg-second-card="<?=$atts['urlsvgsecondcard']?>"

  text-third-card="<?=$atts['textthirdcard']?>"
  subtext-third-card="<?=$atts['subtextthirdcard']?>"
  url-svg-third-card="<?=$atts['urlsvgthirdcard']?>"

  text-fourth-card="<?=$atts['textfourthcard']?>"
  subtext-fourth-card="<?=$atts['subtextfourthcard']?>"
  url-svg-fourth-card="<?=$atts['urlsvgfourthcard']?>"

  text-fiveth-card="<?=$atts['textfivethcard']?>"
  subtext-fiveth-card="<?=$atts['subtextfivethcard']?>"
  url-svg-fiveth-card="<?=$atts['urlsvgfivethcard']?>"

  title-sixth-card="<?=$atts['titlesixthcard']?>"
  text-sixth-card="<?=$atts['textsixthcard']?>"
  subtext-sixth-card="<?=$atts['subtextsixthcard']?>"

  img="<?php echo $atts['img']?>"
  
></Banks-Cards>

