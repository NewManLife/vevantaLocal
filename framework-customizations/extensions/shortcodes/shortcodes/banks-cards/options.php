<?php

$options = [
    'title'   => [
      'label'   => __('Заголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Самые низкие ставки вместе с ВЕВАНТА',
  ],
    'subtitle'   => [
      'label'   => __('Текст формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Мы поможем вам получить самые выгодные условия от наших партнеров и Банков, ниже минимум на 0,7% по рынку.',
  ],
    'buttonText'   => [
      'label'   => __('Текст кнопки', '{domain}'),
      'type'    => 'text',
      'value' => 'получить услугу',
  ],
    'urlButton'   => [
      'label'   => __('Url путь куда направляет кнопка', '{domain}'),
      'type'    => 'text',
      'value' => 'https://vevanta.ru/ipoteka/',
  ],
    'textFirstCard'   => [
      'label'   => __('Текст первой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Скидка нашим клиентам',
  ],
    'subtextFirstCard'   => [
      'label'   => __('Подтекст первой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'до 0,3%',
  ],
    'urlSvgFirstCard'   => [
      'label'   => __('Url картинки первой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'https://upload.wikimedia.org/wikipedia/commons/9/9b/Sberbank_Logo_2020.svg',
  ],
    'textSecondCard'   => [
      'label'   => __('Текст второй карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Скидка нашим клиентам',
  ],
    'subtextSecondCard'   => [
      'label'   => __('Подтекст второй карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'до 0,4%',
  ],
    'urlSvgSecondCard'   => [
      'label'   => __('Url картинки второй карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1e/VTB-Capital_logo_ru_rgb.svg/1024px-VTB-Capital_logo_ru_rgb.svg.png',
  ],
    'textThirdCard'   => [
      'label'   => __('Текст третьей карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Скидка нашим клиентам',
  ],
    'subtextThirdCard'   => [
      'label'   => __('Подтекст третьей карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'до 0,7%',
  ],
    'urlSvgThirdCard'   => [
      'label'   => __('Url картинки третьей карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfUAAABkCAMAAACo21lxAAAAn1BMVEX///8AcXsAa3YAbngAbHcAaHMAb3qy0NMAZXB3rrP0+voAdX7a6Onw9PXY4+WnycyexspUk5rF3d9kl54AdH4whY3n8fIgfYY/hI2It7x+rrPV5+nP3N6awMWfvMDN4uSFq7Bwqa5LlZwAXmpen6U7i5OOur+2y85IlJtEjJSszdCOsbZjoql2pqxamJ+oxsooh4+82tzA0tVtnaQpeoPODYXwAAAa+UlEQVR4nO2de3+iOhOACwFEwNWyglXX9dWKq121tev3/2wvJJnJJAQv3dZ2z8/555y1XPMkk7kl3N2dKVE+fZ3MNsVLURTZajOc/Jjm5557k39Rory3WQ+Y73oMxHXdfnY/a3Wiz364m3yERPvfWeh7AXNMYczz3WKSfvYT3uSdJUraB+bWgRP0LsuWN13/X5J01z+KHMDH2/FnP+pN3km6ReidRC7ECxc3Rf9fkPS+PsyFGed6XhDUuDuz5LMf+SZ/KfnM15gzz/VDb7BajCqZF7FfWnh6r3Cd4Wc/9U3+Srp9VxvI7uD7sKWP5bQ3uo/1ruFmNzX/70q+dYkG97x4k1q1d5S0FoyOeMZuw/1flamjBjpzne3RAZx3dz6x+fziNrv/kzLpq9HrDUanffF0Q85g/f0VnvEm7yxtpd29cHhe/GW6C5F70O998BPe5L0lmvlqkt6eH3ObrnFWCNzlBz7gTT5A5gjPG7QuOTGaMBzubPRRj3eTD5AIobNwZGbToumvn7PV6iV+Wc1HP1tj8+9JgWrCb1/riW/y97JxcaB3tT9EyXJ+6Ls81Row5nkuiw+Llj4DRENUFO7k9M3yaXuxfnjIHh52s/Tr5W+iJP+sLHKed654t6GCPqW/R72dY0biqnCdH//RZ4Elannv6cS98tHAdzFb73+9/E3Lz+bb393p6SPfXQb94s/vpyvdeRnCSF3ToZfMXL8pC1OCn9Bjp+jD+ccduKFrRPk9f/O1CjTaZZ/0XN8PV9d+rnHoVAPBD/0fH3+zFNXzjoAsx6QWnDWFuYMn0izTgYTJBseU9iisX8lffCnsa3iR3bUfawQNfgXqHRim7j359XXg19JrJvewIMoIsbvz5tZaWqCXL7n5uLe7WIbw2t72ynfG0efEHz7rRTsPoCta+c6YzStLrpqNDe6MGPxj6D1eY0w+6Vu7T8C+TFgvaV9kmL6jdFo4SbL7D9cyPel2Uc08zWgWpppq4mKxGS0WL47va/MytQRaOHab7JHnhjmDLT74Jc+U1jZWT+hfM7GwHxVKt/of3t8SaPi+eskeGdOB3/8+IRTz/e81Nci8WI3TltTf7KGhr86IccjoBOJ/vgMXtRa+lk66WhYxSjcuHUtu8eG33IDtolyxIUmpsOypBiRKRgM1AdCsy0i2WoOOjwZImsUPa5K68bvWE64o3bU2pwXu7lquc7pmmqekO1IfIvuw1rNHKrAe3jdMuNFkoML2LiKLCuhD1gfvoC3HqrjAeIDt7H2yPde5NyqI/Nm1DPitfufAv/9w6JF0U1iGP41wSveLIxGXaIhDNQjwuDFQ/GM7Z6p6Cj9jgv2L3duOv5okmW5wuNlFqYi/kGjn63ceXCGD1ZK6pY8FFD0MyLuz451uWkD/YAw9DYjyMZsp1MI37HNLYa/GVpMlcB1ZU+jMdc6oLXgnGfrandlVKk9jMV59zJXt0XvonwqtlsYZHjzAh5VTt7ezHK/G9oFDVo7c51LvqaZnrru+osc2xhuX7qs/uM6qEjn4lNOWxzAxH0zvK6rE+K2H0fcVXlLa5n2L94bU2UFc8gCTSTD4xPqrHO0L5mWz5Jr9bwN2XOAOdul17EeY1T0sgtmBgs6MutjJaFcUxW400WE+AUdVUHEvr2nJuS6RujQjHtCQGXxiDgaHul+0rqtycoDu6lmND5UUhjq8KwxGFtNnSGeO53p8WWv53/6IdogWjFacyMeyDd16A6K77s3FD1+DOjgewXWDcaUsZVtdNQEl9QuO0xzUDS18THeeYeDGG4JoAi46TuSyEf16FR1SZ1+J+jiW7/V89Vvfiwa5QgRWSQ7G1HYkRCr8wFWGXD4Ljdh7RdjfjUCW8CODk1piVZTlVTahJ6J+cl4/Rv3t7RDl+UXaUoaJ3b+o/npjGYbsb8HRLOV7yzMOPVeI1NYk2ZRm9si556LAT8FAnhNJlv3a8E16s/ssrrI4R6hHSbe9e3g4PDwshlPamtFTyyq0aj/vLh4Oh8PgYT0j9T5w5A9+uRyu8ySsp63UePldlC6Hwz0156J8Lw+W//5WnlYeNIMuEk1H84eHQXnDNn3YTp7CTQjRRN55n4jp8FU0XmUC5U/DYe9brfPk6XC3fihlN5xSW6/zA94pohdutfhB8t5pIk8p/1o9825Y/XtjL5lgMd48dSosnhA65pmHgkHWECIb0me35xCq/U4WcV80Gdrwinp3XsWkA1lpU5BIbefRt0modEo+iv1SmwTV2Z7vDOUfOo489JEHIFO4zuM3/udCqKbV3fTeL3uxH+zU/Ja4oTxYXGtUnVseFMpY4nTO9/EI+MPOVf/r4bM+kqjLFi4WPnLs0rr191H54OWdw2KicR9vYyw7cv2MuNITuL4wp/bwT69qxmgt7/MoJtnqr9XSVLdyjyN72tPxMC71xNNAwU6IGpiOV+xA/vSBHKbnO7JnvNioU2k5aAtmYkh0dqGWlQn8DMdKx5qbLw0KaKmuaxggfXnRGH7gfkUKJnsoqMtmGAwg98L8NYyrBE4N9QCDLwAvtRXfLMSwdhd/Z9/xdfNCjRAOay4Do3Mm1WzgUgU50deTl22BdjS6HSJnNoWm4adH0o0C0+oPFD5Ur582tCKOHVEVFTDpqv9UmedNHoH08LwYngky9uzUbLdTKRwZpdnUJhQ2AAKnqC9r58o/KeojC/UILkt0mQd9zaA+waw0/2fPvKML2Al11W0TdaCg/gLBEfq66Bp364vGMapxJvVUf4fq2LZ9yg5BT3Vk8MKT/wbqjC52GKGvO1cT2FC8s3cimE0Ka5jwAFp6UFq0JDgHJ6i/WuYrv3Wa+tR2WbipQT2DUcMnr3F9Mb/UAYS642E1VFe9naBuex2cX3PLhkDeOrqEesh7EAR9RRt/t07rDIyyu5mkbFBnfQUz32GUhhTVDKWSdn8fYx6NiC6XeDJb0RZU3Z6gntUbyWHr09RTS08rJbVQT+EWoq1n9Qb0ZPUYoa7GyFz9KKhb7wzHb214vMnl1LGgsWrj3NZMpG5oKn8IdOqeq2aevJDQA58Mf9TS7FgVZOuevLPUmGSoe6pLgHOgqAfUd5DUyXxFakK4T3Sc+hMNwuOZ0o/TqQMIEX/uKCzqjm5iUmdYI6FsV0E9V3cOGJkS9GlJex+HXUx9Dyky3oxTuzGHxjRErPSx7pNig9SDVgiU9ZqrhTDN1bLTF0/LKrtCfeA878W7e6IIEp16sJ5MJr8f4FhBXVUFsCxTrdk6SV2lXlj2W9kVbp16Bx7P5a+7hxODeIcjSPYWQt0JobnIC5vUnT8jxCFGKI4A5mX3jjID00up/5GPIlyqV6t+wUT3GN+JUvdIsUEXqyMfVHB+nNEiqSbqr7qyljUV0QF0UTaOolQB6OnUReHNdwwKRvSf5fTV6aAqdn9dQr18jTZcRrS9Rh0rDIWJj0tHStWZYzn1ukbdlY4gLRo0qFdaHVNTwiREqyvudqIE80NSFRvUxzggatQ7ssPIUNDEasyhFarGjqLOqAvehhu7KxIPielsJF0jC3Xt3r6cDFPo7h6/IE6DcqZT1J/q1CGRJN8OznV/nk+dq2LELG6qUYdbyOeBB+BBSMxgHPIadTn70fAIp64sCr96A/DrxMADckINYo/x/mejfgdNE/4wqYPbIUNvbasxBxY8mfWRekjC86UdB5G8maI50e1OvylFT5WdKpBtMe2HZ+xWvw3qlrGO1Bmv4gFzmXM+k7pfxeHzdUCvQ6nDQwciYoMtxK+6h9YUGWZKXSqjKCamKqe+R2XBp370kap+G2F0hL/7FJ5DLs1oou6b1CN4H1/M3HOrMcfktK3S/UrDD8jsjZHakLpxhvpwm8pMp/SoAFbc/JKny8HxilnvP8eoi3g/Uhdkf0APP01dX3CCLSYuq4Z+pHwa4dUlUPrJH2eK6cRfJnWpWxM6q+nUxXPhOZVpBTpdhrqSAVz+QurQVSFk9mKjjjGRZzXrA/VXpcj3MKVTN66zNi0Ft2ldszbWS2NQXORZxuXk3PWKgTuRh2yizsMgijrvaUDrEuqiASGQxYqcXKf0IyKcCX7oPZd7nZgp92vUhdrVg0g6dXFOipbJXq0blHoPx/ql1GFacV8ldZtvzGBFHwnhAHUlz2jHkSWw46I2ZTQubjD8h0CE9dLdw6DvovWgKm/+XEz9GzzgaeptnfpWuyxSj5UtJ2OXCIkv58x1I0SjLh5pR0eZMdY5dZwkKsd6vHs4OK4beKIJU1yGdQl1vxXBrFHIIWt13DDdtlDbzdSoY4Wfq6KNd2lc1x2N1JVNCu8i/5Cnv5Y7JjQ8HiMtmEuo/5DHcu/gTOqi66FlzqOfqOFj1ZTSokUfSNjjSF3X1vwJK7teD4/o1EVIaK+ri7sk/TV6kUsAFziBGNRl+UsD9SdQMDjXOjZB6kT/G9QjLChlxI2b2DYbbqZeVIOaWje1KrvxTnluR605O3XeKszl+ZDzqMuAOQZtuOuL1NkMnzUyqY9PUHcqu15Xb8ep0/KO6m45xr1r1J2sKOYFzPqmbYJ/AHPtFHUVGjOpF+hFLFTE5sm/hPpdno+/tcmANy2A8ZaojrdQf/QY6xej186bqccadcyQeJBZxwmIU58eo+7szSV+R6kbRnA0IUUONepVUpmsIdKoq4fGignHJhYNX5vXI1yD7pKaymlcm9ZPLlrEymoIbsgbvBY+baM3UP/2+0ntivku1FHAkVlq1PfHqFcvoLtMNupjI74n23Xr0wFVo66LQb320HcD21lozZGQQt2amzhozSlnLlm7taudoJ4f8MgHtBCifaav+3oLdU3ek3qAUfUG6p6FenWSbjvbqOdQoEKoT3f6vr4O+/4G6ioV0OC5vUilrS6LGj5Vs3gLsDNHFbtE9+Y+BzSAYxVLBVVSu8qXoq4u30R9pVEP+FF+R3jrSPA86tHIMxWoGZszxE7dU6NvZd2KIpDUv9XH+s+MZNsGGK8m6sisa7BHafLpr82+iXrLYhV+Jepq2KBZbVCfU+rsgS8U8Vvd6i6sBwr2KHUwHcb9mvZ8G3Uyf25sYx2NaeVoqNhcSApZc9ygziO13L2+1pXqxdH503aXOT5sSVKjPmGWvng8+yJoqfjox1JXNaBo+RynzuvU2LrqlOwlh3jeceqibnxq8YXfRt054Pw5rHckh9QAqKQ+yb4wUryIO5C6a2XT7fv0hr6x6fSyX31RgDWvgqAVDiRnfQZ1jAB8NHWsPziTugg78JfxZjp1tcRXp874JJET6IGCYVL3tELlBuqq1P/X8UyrqlIgmdbAIyUzEzxAC9DT7R6MTOsM9YOdeodEMtg9WvhvoB71XpMcPjn3Hv66eq65nbr+aoQ6zgSlH65Tz5uo80sQe5qt25iONKizdm8ymeDU2kAd2rvsafbsC1ZCoLWnV1WQyMweXArmkKKqeXNVhVrxNLNS36uOGE5UFPIt1NehHxe737xc/G3U3R8adZx6oCvr1KeNY5241j+S86nnShWHywiD/icyrU2eG46/xOq6KWUAi1ONCiqXLIFTi/2pTdfG7mKufkHqEJQxqKtQdbWeXm2Ddzl1Ps0HzPUf9xdSH2rDBql7bcxjLG3U80bq3+CCwSC5gDqZgPekLQzqMLDs1NlsC7FZmLgx86qLMlJhRzKzWpLq8xUqNqIDllId1aolT1F/0HT661+MdTTuKiTvQd1/AkUEc+C51HF0BetIp54cm9d32FaVIdFIPTsah++mxkOrgipDQjBSc2GZGWO90nWqWCLaYk0C2UNnKSujzaKKE9SxfRgvAEbD46+o352baTWpezr13h1maYz6hBPUO1jv8f1Opx41UV+QqgpWdC6h/qpTv4NSDnQ9Gqx/hhsDiepK2yqIEe5fEC0xB4hZV7kKIvDMsjlFfWlQ50+vKqi4nfzJ1HUbvqQOTy+teMzLG9acFqUpqaulJyOD+l0D9eoxx1hGwTGfS50/G6UOr6PWLdvry0nQVixTgBVPpFjdW3/f/q+S77vvmEtyoURfFugFsQGdUJ+Y1B86apMcuQsalNZ8MHWjAdFjrVGHZRayLKUpNrcxqUPD+63j1BMayldGzeQvqPdULQ0ucyhqpp48AKfoLrfSLasbA7m8Ucv3gEKXBXp1z7lZw3PqMLgDNtXeVNZNv4U6r6U/s24u1mppDM+tbEDVk+jSxOPZl4o61DiHuUkdHQQj5zYiK3lEl38zdbTecOJWG4a4Gleyoqk7sE/+ljWtqjetpT1Q+zBYs+fGnx6rtkRFtaK+P0Zd0DKokxqYC6mD1RvoVRVVmBFVPG8ffFqD+tCkDgVzcWRSR+e4nl83prc3U8eJCKGCExjP9F0LHE/Vwo5XlsQ581/glNEQFD8sxoEnDNaG30b3qhCBYZVzC6ofVIF5cgn1voX6WM47Z1DHfwrX11AhlDoEroSxiVUVvG7OKISh1KUir5wjg/pKK2rDjE1VUKWoCw2PJo+eaZXUo2bqifbQlUgVj6sQYWZhdOeIp7XxaQDP25FRDFuVeegbyGCUJTSqsjOihbsqv16dPcJOwTHjNH8OdVS/InVxAfVc93rUcgatRrZqwAh6lledqOrmqtHYRa+8VaMuhnR1c4M6hgA4WbVAYU+oi8GNCt9OfdpMHcvscUvnnuqFQgzfVUintXKr/SX4ZgDl/22o5sYxhheFoJJlQ2C1jo0XypFSO0EdzTeujeaXUFe0eF0IVEbbqONCEblIQ7nkd4SLWHeuUVfasurQWBLFr4oOjtivUaMuVFwV/jKog3IT3h7akZVFoahzzLM3U8ehhuVKsNEaDvZoBb0505jlTz93RREXxeb3XtPb03rSQ3Zg286wpCTcc1Z9mmqpMsCqGjtuRYn6vpzcp/ZM6lmHtBLTrDlhTrT0sY56lnf+lr7uSKcOeo1fFbVLsO6UDS0vIovfNeryvfY16oi2WleRY4letZZf1Y4HvShSa75lXekF1NGKPwAG2Q+CDFCOYR2dueFc6bN3MJ2hoPc1fXhHbuLUbLlq+YejRDMXeBurfBBzMmJGBgMRTj+LusN2eQcfgoe51PLQw7iq4MbwiqCOw7R0GFGJS49Ep672vE7pHd3XCDck4JgN6uJpqmynQX2MV58QTcdHQKqKK4MHtVqzFL6JyQXU1RJvsNZgTlPr13D7Bbrve5Pgh13J1pDyHmxlO6FhJxyZ1m+RjqD1CRY+n6JOVuOzQJVhuluNOhv8HqlLS+pkSQbas47z2KlTv9Ni8aiRg0GBLStVnEadz4J8qBnUVcfpb0d4Z24dJmpT9UD/AgdPeVxCHeclLJmExW5qOxRlSHsnNsvOFzA2A1bbfJhZhrrOlYrHy/Ua1lY7slb4OHVaLqDCSXx9QaT2gyntEtJ+cvGlSlGprZU8YesY1DGs/RJpK+bV8j5ZUqBRv9sN+nG2qVMnD81UOUnM+5s9S+LIgtZLqCfwcLjLFC6nwgQsWazmro8N966q11XFFoksprHvFG7fUMIBoz0/NL3pGdTH1kjjQbOVDQHqlt0qAvmRBIM6zuV8rWB9sQ9Wm+rU8zxJ8txCPbc0h6yaWzSpxYup4yZIIeKEha6hKnZSO5azcNH0CfbWGncfDMiHHNYeaRWLpFbqvgza6HlhMiw5g+PU7xaW2iBXlAl2tT+pHQBgofWshh1KvAzq6Fxyg3jsmG/DHDnT6dRRTOq4Mye5hByRe60Xk6rJi6mr3duxaBK/3qB2iI/I0lTPWVu+AJJPCrKjBtlSfAkuQOPnWif1ceX4W6l6prQNvOVMKZMzqI8HtR7F5LZYOV1E7KlNIYB6ZI5aD5aFmdRBq4sQVMvArqpLkDqt87dQr226xWLJIVqRZ3K3bTULXUwd/kqqXKCkhkTfSyVPKpn8+I+2UfR0suiT4mVaRwMp6CBr3vN6aXy7kbnk4yPDEOO7/pIU4p5DvbTKTHa4nZbyfIKwjQar2lShM6f7pgZhUdtvDgo/8RbiswbaFruuqhNE6vqKgDp1fcu6wFcbrE+xnVilC9GIupw6fAUAF7beqXncIxHUlhZ+L8G4g/liuBwt5rH5Za9CqYIUTHqzSlKT6puQfMPE6hNxPlsvqc046fOrMz+rLpEXVXFlFfOvZqTOo9zD9JGvDP7uw5am0IL5Tm0nFJTX2Cnnc8K/NstYmLXKJmK8xBD2lhRvXH24qgpEeS798EniyJvAnLgJ5b9FV41G1Xdsgyob5fe3qq935WG+spf45QbwCqTVNmUrlHcOAs8fDEljjFd8DDB/UN072olPJLOQU4fri+E7haYJBXXZNDBtP8PRyrHKQce7JK6SmzqPedVms8wLjF9J9aQKhZ/46GH+bbIoDqUU7V5iaIW8fYj7/awrfu60Nqtstdq0ecn7rC1ENM2kDaKWzYzbh77YFJf1d1o8KV0c4kM2rzaX7RTlBcurdHvazqxpmz/TfPmD/JzDPeDLHFN8CHjr5fwgzqMBjqQ7FIfpteHR0jibS9kcvDVm5qeuJ0U/jouleJ4onc2L1Woxq/pkqrdFDv+cpfw8+q/q3bri3yNS4jwGp8GlS1WeA2vltMbcL8igVpHw4u/2vc6Tv9lCedx6/jn6+byvX+MDv6/9YVe+cPvrS6QLGP02efwzvs7bJYcnUBLNPvNjHjc5X0Yq1E9/TmaBbX2yONKPn2k3HGMmxb/WR7Fu8peCvq5rhN9798z3zNgJt1z0AE4XvWBPn8lu8nUFU0aOl+k4S998fugHLm5TXtpJ2cI0O5YYKvZHdzf5VyTCxYralrDyj9PWz9FitSotyN3wuVULu+W4JWzgnlq7fJMvJSqq7mZNH9K2StTFCFXwN99OuckniBqwJcTR+a7IlGxR4d9G+j8nKuztuMGZ346cLlQoM+jfDLl/UCYkfe8Nhie5R+mmT/Ji8UUTw02+iuzVxinVl4m3R6tp8u49LZ71zZKrm/wrkv+hiVDP629erR+r7SRPc4/ulOM5TRsF3+QfkElfS7t4/uB+1NN8tU462a77eo28mx3Lst3ky0u+NaKwzPXDMH6Zrxaj2WoVx6HvGtE6t38b6P+8pPeW3b+YV62AYJYtorx4dpvR/wvSLSyf5LUKcx8XN9P9vyLTjWPbBNpk7g9Gn/jd9Ju8uySjLDgKvnTtsu41PyF9k6vIdFn4vsdsk7zrs93zTbX/NyXKW9UX06sKSebx71uXvJ04+z7cN1fB3uS/IPn0x6/lbLNZvew2m9nkdTq+6fV/Vf4Pxf8IuiPNRq0AAAAASUVORK5CYII=',
  ],
    'textFourthCard'   => [
      'label'   => __('Текст четвертой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Скидка нашим клиентам',
  ],
    'subtextFourthCard'   => [
      'label'   => __('Подтекст четвертой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'до 0,4%',
  ],
    'urlSvgFourthCard'   => [
      'label'   => __('Url картинки четвертой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'https://cdn.open.ru/packs/media/src/front/images/logo/desktop/bank_black-c8b1092a477e1547bd4878fbb6aa116e.svg',
  ],
    'textFivethCard'   => [
      'label'   => __('Текст пятой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Скидка нашим клиентам',
  ],
    'subtextFivethCard'   => [
      'label'   => __('Подтекст пятой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'до 0,4%',
  ],
    'urlSvgFivethCard'   => [
      'label'   => __('Url картинки пятой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'https://upload.wikimedia.org/wikipedia/commons/a/a1/PSB_logo_ru.png',
  ],
    'titleSixthCard'   => [
      'label'   => __('Заголовок шестой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Наши партнеры',
  ],
    'textSixthCard'   => [
      'label'   => __('Текст шестой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Скидка нашим клиентам',
  ],
    'subtextSixthCard'   => [
      'label'   => __('Подтекст шестой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'до 0,7%',
  ],
    'img'   => [
      'label'   => __('Картинка-фон формы', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
];
