<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( '4 карточки с ссылкой', 'fw' ),
	'description' => __( 'Компонент для отображения карточек', 'fw' ),
];
