<?php

$options = [
  'title'   => [
      'label'   => __('Заголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'С нами легко и удобно',
  ],

  'titleCardFirst'   => [
      'label'   => __('Заголовок первой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Ипотека прямо из дома',
  ],

  'subtitleCardFirst'   => [
      'label'   => __('Подзаголовок первой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Оставьте заявку и мы оформим для вас ипотеку дистанционно, электронная регистрация сделки с ипотекой не требует вашего визита в МФЦ',
  ],

  'urlCardFirst'   => [
      'label'   => __('Url первой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'https://vevanta.ru/jelektronnaja-registracija-sdelki/',
  ],

  'titleCardSecond'   => [
      'label'   => __('Заголовок второй карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Безопасная сделка',
  ],

  'subtitleCardSecond'   => [
      'label'   => __('Подзаголовок второй карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Наши юристы и служба безопасности проверяют сделки со 100% гарантией безопасности',
  ],

  'urlCardSecond'   => [
      'label'   => __('Url второй карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'https://vevanta.ru/yuridicheskie-uslugi/',
  ],

  'titleCardThird'   => [
      'label'   => __('Заголовок третьей карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Самые низкие ставки',
  ],

  'subtitleCardThird'   => [
      'label'   => __('Подзаголовок третьей карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Только для наших клиентов у нас есть партнерские программы и эксклюзивные предложения банков',
  ],

  'urlCardThird'   => [
      'label'   => __('Url третьей карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'https://vevanta.ru/refinansirovanie-ipotechnogo-kredita/',
  ],

  'titleCardFourth'   => [
      'label'   => __('Заголовок четвертой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Личный менеджер',
  ],

  'subtitleCardFourth'   => [
      'label'   => __('Подзаголовок четвертой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Ваш персональный менеджер подберет для вас идельный вариант ипотеки и организует экскурсию по обьектам заинтересовавшей вас недвижимости',
  ],

  'urlCardFourth'   => [
      'label'   => __('Url четвертой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'https://vevanta.ru/kontakty/',
  ],

  'img'   => [
      'label'   => __('Картинка-фон формы', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
  
];
