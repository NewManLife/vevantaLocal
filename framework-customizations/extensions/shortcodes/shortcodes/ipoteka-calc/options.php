<?php

$options = [
  'price'   => [
      'label'   => __('Стоимость', '{domain}'),
      'type'    => 'text',
      'value' => 1000000,
  ],
  'percent'   => [
      'label'   => __('Процент первоначального взноса', '{domain}'),
      'type'    => 'text',
      'value' => 20,
  ],

  'position' => [
    'type'  => 'radio',
    'value' => 'center',
    'attr'  => ['class' => 'custom-class', 'data-foo' => 'bar'],
    'label' => __('Расположение формы', '{domain}'),
    'choices' => [
        'left' => __('слево', '{domain}'),
        'right' => __('Справа', '{domain}'),
        'center' => __('По центру', '{domain}'),
    ],
    'inline' => true,
  ],

  'btn_name'   => [
      'label'   => __('Название кнопки', '{domain}'),
      'type'    => 'text',
      'value' => 'Индивидуальный расчет',
  ],
  'class_id'   => [
      'label'   => __('Class объекта(Тип недвижимости)', '{domain}'),
      'type'    => 'text',
      'value' => false,
  ],
  'staff_id'   => [
      'label'   => __('ID ответственного', '{domain}'),
      'type'    => 'text',
      'help' => '48 по умолчанию Марина Михно',
      'value' => 48,
  ],
  'manager_id'   => [
      'label'   => __('ID менеджера', '{domain}'),
      'type'    => 'text',
      'help' => '0 - Отключен',
      'value' => 0,
  ],
];
