<?php
$options = [
    'title'   => [
      'label'   => __('Заголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Центр обучения и развития навыков',
  ],
    'img'   => [
      'label'   => __('Url картинки фона формы', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
  'subtitle'   => [
      'label'   => __('Подзаголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'В Центре недвижимости Веванта работает собственный Центр обучения и развития навыков. Специалистами Центра сформирована авторская прорамма обучения как для новичков в сфере недвижимости, так и для опытных риельторов и менеджеров групп. Специалисты центра проводят не только обучение в группах, но и индивидуальные консультации для риелторов, которым необходимо усилить определенный навык.',
  ],
  'classAgent'   => [
      'label'   => __('Отображение карточек на одном уровне с текстом пунктов', '{domain}'),
      'type'    => 'text',
      'value' => 'uk-width-1-1 uk-width-1-2@m',
  ],
  'classTextRigth'   => [
      'label'   => __('Отображение текста на одном уровне с карточками', '{domain}'),
      'type'    => 'text',
      'value' => 'uk-width-1-1 uk-width-1-2@m',
  ],
  'classAgentDiv'   => [
      'label'   => __('Количество карточек на одном уровне', '{domain}'),
      'type'    => 'text',
      'value' => 'uk-width-1-1 uk-width-1-2@s uk-text-center',
  ],
    'cards' => array(
        'type' => 'addable-popup',
        'label' => __('Карточка', '{domain}'),
        'popup-title' => null,
        'size' => 'small', // small, medium, large
        'template' => '{{- titleAgent }}',
        'limit' => 0, // limit the number of popup`s that can be added
        'add-button-text' => __('Добавить', '{domain}'),
        'sortable' => true,
        'fw-storage' => [
        'type' => 'post-meta',
        'post-meta' => 'comments',
        ],
        'popup-options' => array(
                'titleAgent' => array(
                    'label' => __('Заголовок карточки', '{domain}'),
                    'type' => 'text',
                    'value' => 'Игорь Приписнов',
                ),
                'subtitleAgent' => array(
                    'label' => __('Подзаголовок карточки', '{domain}'),
                    'type' => 'text',
                    'value' => 'Специалист по развитию и обучению',
                ),
                'imgAgent' => array(
                    'label' => __('Url картинки карточки 350х350', '{domain}'),
                    'type' => 'text',
                    'value' => 'https://vevanta.ru/wp-content/uploads/2021/02/Слой-11.png',
                ),
            ),
        ),
    'titleDown'   => [
      'label'   => __('Заголовок текста после карточек', '{domain}'),
      'type'    => 'text',
      'value' => 'Авторская программа обучения',
  ],
    'textDown'   => [
      'label'   => __('Текст после карточек', '{domain}'),
      'type'    => 'text',
      'value' => 'Специалистами Центра обучения и развития навыков разработана программа, которая включает в себя теорию и практические заятия, позволяющие закрепить полученные знания.',
  ],
  'titleRigth'   => [
      'label'   => __('Заголовок текста с пунктами', '{domain}'),
      'type'    => 'text',
      'value' => 'Темы, которые входят в программу:',
  ],
    'texts' => array(
        'type' => 'addable-popup',
        'label' => __('Пункты текста', '{domain}'),
        'popup-title' => null,
        'size' => 'small', // small, medium, large
        'template' => '{{- message }}',
        'limit' => 0, // limit the number of popup`s that can be added
        'add-button-text' => __('Добавить', '{domain}'),
        'sortable' => true,
        'fw-storage' => [
        'type' => 'post-meta',
        'post-meta' => 'comments',
        ],
        'popup-options' => array(
                'message' => array(
                    'label' => __('Пункт текста', '{domain}'),
                    'type' => 'text',
                    'value' => '- Воронка продаж по направлениям',
                ),
            ),
        ),
'classTitle'   => [
      'label'   => __('Класс стиля заголовка', '{domain}'),
      'type'    => 'text',
      'value' => 'fs18 fs20@s fs24@m fs28@l fnt-bld',
  ],
'classSubtitle'   => [
      'label'   => __('Класс стиля подзаголовка', '{domain}'),
      'type'    => 'text',
      'value' => 'uk-margin-top fs16 fs18@l',
  ],
'classTitleAgent'   => [
      'label'   => __('Класс стиля заголовка карточки агента', '{domain}'),
      'type'    => 'text',
      'value' => 'fs18 fs20@s fs24@l fnt-bld uk-margin-top',
  ],
'classSubtitleAgent'   => [
      'label'   => __('Класс стиля подзаголовка карточки агента', '{domain}'),
      'type'    => 'text',
      'value' => 'fs16 fs18@s',
  ],
'classTitleDown'   => [
      'label'   => __('Класс стиля заголовка текста после карточек', '{domain}'),
      'type'    => 'text',
      'value' => 'fs18 fs20@s fs24@l fnt-bld',
  ],
'classTextDown'   => [
      'label'   => __('Класс стиля текста после карточек', '{domain}'),
      'type'    => 'text',
      'value' => 'fs16 fs18@s',
  ],
'classTitleRigth'   => [
      'label'   => __('Класс стиля заголовка текста справа от карточек', '{domain}'),
      'type'    => 'text',
      'value' => 'fs18 fs20@s fs24@l fnt-bld',
  ],
'classMessage'   => [
      'label'   => __('Класс стиля текста справа от карточек', '{domain}'),
      'type'    => 'text',
      'value' => 'fs16 fs18@s',
  ],
];
