<Img-Text-Block

title="<?=$atts['title']?>"
img="<?=$atts['img']?>"
subtitle="<?=$atts['subtitle']?>"
class-agent="<?=$atts['classagent']?>"
class-text-rigth="<?=$atts['classtextrigth']?>"
class-agent-div="<?=$atts['classagentdiv']?>"
title-down="<?=$atts['titledown']?>"
text-down="<?=$atts['textdown']?>"
title-rigth="<?=$atts['titlerigth']?>"

class-title="<?=$atts['classtitle']?>"
class-subtitle="<?=$atts['classsubtitle']?>"
class-title-agent="<?=$atts['classtitleagent']?>"
class-subtitle-agent="<?=$atts['classsubtitleagent']?>"
class-title-down="<?=$atts['classtitledown']?>"
class-text-down="<?=$atts['classtextdown']?>"
class-title-rigth="<?=$atts['classtitlerigth']?>"
class-message="<?=$atts['classmessage']?>"


cards="<?php echo htmlspecialchars(json_encode($atts['cards']), ENT_QUOTES, 'UTF-8'); ?>"
texts="<?php echo htmlspecialchars(json_encode($atts['texts']), ENT_QUOTES, 'UTF-8'); ?>"

></Img-Text-Block>