<?php

$options = [
  'position' => [
    'type'  => 'radio',
    'value' => 'center',
    'attr'  => ['class' => 'custom-class', 'data-foo' => 'bar'],
    'label' => __('Расположение формы', '{domain}'),
    'choices' => [
        'left' => __('слево', '{domain}'),
        'right' => __('Справа', '{domain}'),
        'center' => __('По центру', '{domain}'),
    ],
    'inline' => true,
  ],
];
