<?php
  $position = '';
  $width = 'uk-width-1-1';

  if ($atts['position'] === 'left') {
    $width = 'uk-width-2-3@l';
    $position = 'uk-align-left';
  }

  if ($atts['position'] === 'right') {
    $width = 'uk-width-2-3@l';
    $position = 'uk-align-right';
  }
?>


<div class="<?php echo $width?> <?php echo $position;?>">
  <New-House-Card
  
  ></New-House-Card>
</div>