<?php
$options = [
  'galery' => array(
    'type' => 'addable-popup',
    'label' => __('Галерея', '{domain}'),
    'template' => '{{- img_name }}',
    'popup-title' => null,
    'size' => 'small', // small, medium, large
    'limit' => 0, // limit the number of popup`s that can be added
    'add-button-text' => __('Добавить каринку', '{domain}'),
    'sortable' => true,
    'fw-storage' => [
      'type' => 'post-meta',
      'post-meta' => 'comments',
    ],
    'popup-options' => array(
        'galery_img' => array(
          'type'  => 'upload',
          'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
          'label' => __('Картинка', '{domain}'),
          'images_only' => true,
          'files_ext' => array( 'png', 'jpg', 'jpeg' ),
          'extra_mime_types' => array( 'audio/x-aiff, aif aiff' )
          ),
          'img_name' => array(
            'label' => __('Название', '{domain}'),
            'type' => 'text',
            'value' => '',
          ),

        ),
    ),
];
