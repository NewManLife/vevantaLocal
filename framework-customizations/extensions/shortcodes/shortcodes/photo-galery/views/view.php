<section>
  <div class="uk-container">
    <div uk-slider>
      <div class="uk-position-relative">
        <div class="uk-slider-container uk-light">

          <div class="uk-slider-items uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m uk-grid">
            <?php foreach ($atts['galery'] as $galery): ?>
              <div>
                <div class="uk-panel">
                  <img src="<?=$galery['galery_img']['url'];?>" alt="">
                </div>
              </div>
            <?php endforeach;?>
          </div>
        </div>

        <div class="uk-hidden@s uk-light">
            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>

        <div class="uk-visible@s uk-dark">
            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>
      </div>
          
      <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

    </div>
  </div>
</section>
