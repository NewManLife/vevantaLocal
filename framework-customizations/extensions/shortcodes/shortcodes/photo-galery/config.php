<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Фото галерея', 'fw' ),
	'description' => __( 'Шорткод для вывода фотогалерии', 'fw' ),
];
