<?php
$options = [

  'items' => array(
    'type'  => 'addable-box',
    'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
    'label' => __('Пункт списка', '{domain}'),
    'box-options' => array(
      'title'  => [
        'type' => 'text',
        'label' => 'Описание'
      ],
      'picture' => [
        'type' => 'text',
        'label' => 'URL изображения'
      ]
    ),
    'template' => '{{- title }}', // box title
    'add-button-text' => __('Добавить', '{domain}'),
    'sortable' => true,
  )
];

?>
