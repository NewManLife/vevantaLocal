<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Hero + форма отправки данных и документа', 'fw' ),
	'description' => __( 'Компонент для вывода hero с формой отправки данных и приклеплением документа', 'fw' ),
];
