<Hero-Middle

  title="<?=$atts['title']?>"

  img="<?php echo $atts['img']?>"

  img-upload="<?php echo $atts['imgupload']?>"

  text-upload="<?=$atts['textupload']?>"

  button-text="<?=$atts['buttontext']?>"

  style-dark-div="<?=$atts['styledarkdiv']?>"

  style-text="<?=$atts['styletext']?>"

  style-politika="<?=$atts['stylepolitika']?>"

  style-politika-input="<?=$atts['stylepolitikainput']?>"

  style-politika-link="<?=$atts['stylepolitikalink']?>"

  staff-id="<?=$atts['staffid']?>"

  manager-id="<?=$atts['managerid']?>"

  class-id="<?=$atts['classid']?>"
  
  source-id="<?=$atts['sourceid']?>"

></Hero-Middle>
