<?php

$options = [
  'title'   => [
      'label'   => __('Заголовок', '{domain}'),
      'type'    => 'text',
      'value' => 'Хочу в команду ЦН Веванта',
  ],

  'img'   => [
      'label'   => __('Url картинки фона формы', '{domain}'),
      'type'    => 'text',
      'value' => 'https://vevanta.ru/wp-content/uploads/2020/10/Работа-в-компании.jpg',
  ],

  'imgUpload'   => [
      'label'   => __('Url картинки отправки документа 15х15', '{domain}'),
      'type'    => 'text',
      'value' => 'https://vevanta.ru/wp-content/uploads/2021/02/clip.png',
  ],

  'textUpload'   => [
      'label'   => __('Текст отправки документа', '{domain}'),
      'type'    => 'text',
      'value' => 'Прикрепить резюме',
  ],

  'buttonText'   => [
      'label'   => __('Текст кнопки', '{domain}'),
      'type'    => 'text',
      'value' => 'Отправить резюме',
  ],

  'styleDarkDiv'   => [
      'label'   => __('Стиль темного фона', '{domain}'),
      'type'    => 'text',
      'value' => 'background: rgba(32, 48, 71, 0.8);',
  ],

  'styleText'   => [
      'label'   => __('Стиль текста', '{domain}'),
      'type'    => 'text',
      'value' => 'color: #fff;',
  ],

  'stylePolitika'   => [
      'label'   => __('Стиль текста политики', '{domain}'),
      'type'    => 'text',
      'value' => 'color: #fff; border: none;',
  ],
  'stylePolitikaInput'   => [
      'label'   => __('Стиль checkbox политики', '{domain}'),
      'type'    => 'text',
      'value' => 'background-color: #009688;border: none;',
  ],
  'stylePolitikaLink'   => [
      'label'   => __('Стиль ссылки политики', '{domain}'),
      'type'    => 'text',
      'value' => 'color: #fff; border: none;',
  ],
  'staffId'   => [
      'label'   => __('staffId', '{domain}'),
      'type'    => 'text',
      'value' => '110',
  ],
    'managerId'   => [
      'label'   => __('managerId', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
    'classId'   => [
      'label'   => __('classId', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
    'sourceId'   => [
      'label'   => __('sourceId', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],

];
