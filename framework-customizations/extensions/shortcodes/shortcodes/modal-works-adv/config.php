<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Модальное окно для отклика на вакансию', 'fw' ),
	'description' => __( 'Модальное окно для отправки информации по вакансии', 'fw' ),
];
