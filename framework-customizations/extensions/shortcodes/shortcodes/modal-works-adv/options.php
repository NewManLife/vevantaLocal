<?php
$options = [
'title'   => [
      'label'   => __('Заголовок', '{domain}'),
      'type'    => 'text',
      'value' => 'Отклик на вакансию специалиста',
  ],
  'imgUpload'   => [
      'label'   => __('Картинка загрузки файлов', '{domain}'),
      'type'    => 'text',
      'value' => 'https://vevanta.ru/wp-content/uploads/2021/02/clip.png',
  ],
  'textUpload'   => [
      'label'   => __('Текст загрузки файлов', '{domain}'),
      'type'    => 'text',
      'value' => 'Прикрепить резюме',
  ],
  'styleText'   => [
      'label'   => __('Стиль текста загрузки файлов', '{domain}'),
      'type'    => 'text',
      'value' => 'color: #5d5d5d;',
  ],
  'subtitle'   => [
      'label'   => __('Подаголовок', '{domain}'),
      'type'    => 'text',
      'value' => 'Мы помогаем найти вам идеальную вакансию, с учетом ваших способностей.',
  ],
'staffId'   => [
      'label'   => __('staffId', '{domain}'),
      'type'    => 'text',
      'value' => '110',
  ],
'managerId'   => [
      'label'   => __('managerId', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
'classId'   => [
      'label'   => __('classId', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
'sourceId'   => [
      'label'   => __('sourceId', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],

];
