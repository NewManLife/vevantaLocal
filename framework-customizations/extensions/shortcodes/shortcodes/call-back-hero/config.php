<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Форма Hero обратного звонка', 'fw' ),
	'description' => __( 'Компонент для Hero обратного звонка', 'fw' ),
];
