<Call-Back-Hero
  title-hero="<?=$atts['titlehero']?>"
  text-hero="<?=$atts['texthero']?>"
  img="<?php echo $atts['img']?>"
  button-hero="<?php echo $atts['buttonhero']?>"
  
  <?php if ($atts['staff_id']) {?>
    :staff-id="<?php echo $atts['staff_id'];?>"
  <?php }?>
  <?php if ($atts['manager_id'] || $atts['manager_id'] === '0') {?>
    :manager-id="<?php echo $atts['manager_id'];?>"
  <?php }?>
  <?php if ($atts['class_id']) {?>
    :class-id="<?php echo $atts['class_id'];?>"
  <?php }?>

></Call-Back-Hero>

