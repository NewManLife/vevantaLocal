<?php

$options = [
    'titleHero'   => [
      'label'   => __('Заголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Возврат налогового вычета',
  ],
    'textHero'   => [
      'label'   => __('Подзаголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Мы вам поможем получить налоговый вычет и возмем на себя все бумажные хлопоты по подготовке документов.',
  ],
    'img'   => [
      'label'   => __('Картинка-фон формы', '{domain}'),
      'type'    => 'text',
      'value' => 'https://www.prodespachos.com/wp-content/uploads/2020/07/C40A01B0-044C-4D0C-B6C6-1F7F8509D556.jpeg',
  ],
    'buttonHero'   => [
      'label'   => __('Текст кнопки отправки формы', '{domain}'),
      'type'    => 'text',
      'value' => 'получить услугу',
  ],
  'class_id'   => [
      'label'   => __('Class объекта(Тип недвижимости)', '{domain}'),
      'type'    => 'text',
      'value' => false,
  ],

  'staff_id'   => [
      'label'   => __('ID ответственного', '{domain}'),
      'type'    => 'text',
      'help' => '48 по умолчанию Марина Михно',
      'value' => 48,
  ],
  
  'manager_id'   => [
      'label'   => __('ID менеджера', '{domain}'),
      'type'    => 'text',
      'help' => '0 - Отключен',
      'value' => 0,
  ],  
  
];
