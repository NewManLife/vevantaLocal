<?php
$options = [
  'photos' => [
    'type'  => 'multi-upload',
    'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
    'label' => __('Изображения', '{domain}'),
    'images_only' => true,
    'files_ext' => array( 'webp', 'jpeg',  'jpg',  'png',  'svg'),
    'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
  ],
  'youtube'   => [
    'label'   => __('Ссылка на видео youtube', '{domain}'),
    'type'    => 'text',
    'value' => '',
],
];
