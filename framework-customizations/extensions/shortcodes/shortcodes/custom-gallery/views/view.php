<galery
  photos="<?php echo htmlspecialchars(json_encode($atts['photos']), ENT_QUOTES, 'UTF-8'); ?>"
  cdn="https://"
  background_size_class="uk-background-cover"
  youtube="<?php echo $atts['youtube']?>"
></galery>
