<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Галлерея', 'fw' ),
	'description' => __( 'Галлерея', 'fw' ),
	'tab'         => __( 'Формы', 'forms' ),
];
