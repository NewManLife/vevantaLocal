<Testimonials
  title="<?=$atts['title']?>"
  subtitle="<?php echo $atts['subtitle']?>"
  img="<?php echo $atts['img']?>"
  button-text="<?php echo $atts['buttontext']?>"
  url-button="<?php echo $atts['urlbutton']?>"

  cards="<?php echo htmlspecialchars(json_encode($atts['cards']), ENT_QUOTES, 'UTF-8'); ?>"

  title-fourth-card="<?=$atts['titlefourthcard']?>"
  text-fourth-card="<?=$atts['textfourthcard']?>"
  button-testimonials="<?php echo $atts['buttontestimonials']?>"
  target-button-testimonials="<?php echo $atts['targetbuttontestimonials']?>"
  url-button-testimonials="<?php echo $atts['urlbuttontestimonials']?>"

></Testimonials>
