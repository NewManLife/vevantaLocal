<?php

$options = [
    'title'   => [
      'label'   => __('Заголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => '99% наших клиентов довольны нашими услугами и специалистами',
  ],

    'subtitle'   => [
      'label'   => __('Подзаголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'В компании ВЕВАНТА работают высоко квалифицированные сотрудники, способные помочь вам с поиском и приобретением недвижимости согласно вашим запросов и с учетом предостовления вам максимальных скидок от наших партнеров. Плюс мы работаем там где вам удобно.',
  ],

    'img'   => [
      'label'   => __('Картинка-фон формы', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],

    'buttonText'   => [
      'label'   => __('Текст кнопки формы', '{domain}'),
      'type'    => 'text',
      'value' => 'наши специалисты',
  ],

    'urlButton'   => [
      'label'   => __('Url ссылка кнопки формы', '{domain}'),
      'type'    => 'text',
      'value' => 'https://vevanta.ru/kontakty/',
  ],
  'cards' => array(
        'type' => 'addable-popup',
        'label' => __('Добавить карточку', '{domain}'),
        'popup-title' => null,
        'size' => 'small', // small, medium, large
        'template' => '{{- subtitleFirstCard }}',
        'limit' => 0, // limit the number of popup`s that can be added
        'add-button-text' => __('Добавить', '{domain}'),
        'sortable' => true,
        'fw-storage' => [
        'type' => 'post-meta',
        'post-meta' => 'comments',
        ],
        'popup-options' => array(
                'titleFirstCard' => array(
                    'label' => __('Заголовок первой карточки = имя клиента', '{domain}'),
                    'type' => 'text',
                    'value' => 'Евгений',
                ),
                'dateFirstCard' => array(
                    'label' => __('Дата публикации отзыва первой карточки, в формате "Месяц DD, YYYY"', '{domain}'),
                    'type' => 'text',
                    'value' => 'Июль 19, 2019',
                ),
                'textFirstCard' => array(
                    'label' => __('Текст отзыва первой карточки ', '{domain}'),
                    'type' => 'text',
                    'value' => 'Лилия нам хорошо помогла когда мы искали новую квартиру, так как у нас появились дети нам нужна была квартира побольше и она предложила нам продать нашу старую квартиру и взять выгодный кредит в банке + скидка на ремонт от партнера Веванты',
                ),
                'subtitleFirstCard' => array(
                    'label' => __('Подзаголовок первой карточки = имя агента ', '{domain}'),
                    'type' => 'text',
                    'value' => 'Лобанова Лилия Владимировна',
                ),
                'urlFirstCard' => array(
                    'label' => __('Url ссылка футера первой карточки = id агента', '{domain}'),
                    'type' => 'text',
                    'value' => 'https://vevanta.ru/agent/?id=84',
                ),
                'urlImgFirstCard' => array(
                    'label' => __('Url ссылка картинки 80х80 первой карточки', '{domain}'),
                    'type' => 'text',
                    'value' => 'https://cloud.vevanta.ru//photo/users/84/fe587a3b181e538bdf2b3ad63b30d323.jpg',
                ),
            ),
        ),

    'titleFourthCard'   => [
      'label'   => __('Заголовок четвертой карточки = карточка обратной связи', '{domain}'),
      'type'    => 'text',
      'value' => 'Вам понравилось работать с нами',
  ],
    'textFourthCard'   => [
      'label'   => __('Текст четвертой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Поделитесь с нами вашим мнением о работе нашего специалиста и в дальнейшим мы обезательно порадуем вас новыми специальными предложениями и скидками от наших партнеров.',
  ],
    'buttonTestimonials'   => [
      'label'   => __('Текст кнопки четвертой карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'отставить отзыв',
  ],
    'targetButtonTestimonials'   => [
      'label'   => __('Таргет кнопки = подключает модальное окно', '{domain}'),
      'type'    => 'text',
      'value' => 'target: #modal-feedback',
  ],
    'urlButtonTestimonials'   => [
      'label'   => __('Url ссылка кнопки четвертой карточки', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
  
];
