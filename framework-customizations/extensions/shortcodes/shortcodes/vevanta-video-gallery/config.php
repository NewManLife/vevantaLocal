<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Галерея 5 видео', 'fw' ),
	'description' => __( 'Компонент для видео галерии', 'fw' ),
];
