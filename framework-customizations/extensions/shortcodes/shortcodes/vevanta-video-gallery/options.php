<?php

$options = [
  'cards' => array(
        'type' => 'addable-popup',
        'label' => __('Видео', '{domain}'),
        'popup-title' => null,
        'size' => 'small', // small, medium, large
        'template' => '{{- youtubeVideoName }}',
        'limit' => 0, // limit the number of popup`s that can be added
        'add-button-text' => __('Добавить', '{domain}'),
        'sortable' => true,
        'fw-storage' => [
        'type' => 'post-meta',
        'post-meta' => 'comments',
        ],
        'popup-options' => array(
                'youtubeVideoName' => array(
                    'label' => __('Название видео', '{domain}'),
                    'type' => 'text',
                    'value' => '',
                ),
                'youtubeVideo' => array(
                    'label' => __('Youtube ссылка', '{domain}'),
                    'type' => 'text',
                    'value' => 'https://www.youtube.com/watch?v=On2puuxf4ms',
                ),

            ),
        ),
];
