<section class="uk-container uk-margin-medium-top">
  <?php if ($atts['panel4_advantage_block_title']) {?>
  <div class="title">
    <?php echo $atts['panel4_advantage_block_title'];?>
  </div>
  <?php }?>
  <div class="uk-grid uk-grid-small uk-child-width-1-1 uk-child-width-1-2@m uk-child-width-1-3@l">
    <?php foreach ($atts['panel4_advantages'] as $item) {?>
    <div class="uk-left uk-margin-medium-bottom@m uk-grid uk-grid-collapse">
      <div class="uk-width-1-6 uk-width-1-4@m mr15 ">
        <img data-src="<?php echo $item['panel4_advantage_icon']['url'];?>" alt="" uk-img>
      </div>
      <div class="uk-width-expand">
        <div class="fnt-bld fs18@m cl-dark uk-margin-small-bottom">
          <?php echo $item['panel4_advantage_title'];?>
        </div>
        <div class="">
          <?php echo $item['panel4_advantage_desc'];?>
        </div>
      </div>
    </div>
    <div class="uk-hidden@s uk-margin">
      <hr>
    </div>
    <?php }?>
  </div>
</section>
