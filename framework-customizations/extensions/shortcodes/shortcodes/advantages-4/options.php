<?php

$options = [
  'panel4_advantage_block_title'  => [
    'type' => 'text',
    'label' => 'Заголовок',
    'max' => 2,
    'fw-storage' => [
      'type' => 'post-meta',
      'post-meta' => 'panel4_advantage_block_title',
    ],
  ],
  'panel4_advantages' => array(
      'type'  => 'addable-box',
      'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
      'label' => __('Преимущество', '{domain}'),
      'box-options' => array(
        'panel4_advantage_title'  => [
          'type' => 'text',
          'label' => 'Заголовок',
          'max' => 2,
        ],
        'panel4_advantage_desc' => array(
          'type' => 'textarea', 'label' => 'Описание',
        ),
        'panel4_advantage_icon' => array(
            'type'  => 'upload',
            'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
            'label' => __('Иконка', '{domain}'),
            'desc' => 'Размер изображения: 125x125',
            'images_only' => true,
            'files_ext' => array( 'webp', 'jpeg',  'jpg',  'png',  'svg'),
            'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
        )
      ),
      'fw-storage' => [
        'type' => 'post-meta',
        'post-meta' => 'panel4_advantages',
      ],
      'template' => '{{- panel4_advantage_title }}', // box title
      'limit' => 0, // limit the number of boxes that can be added
      'add-button-text' => __('Добавить', '{domain}'),
      'sortable' => true,
  )
];
