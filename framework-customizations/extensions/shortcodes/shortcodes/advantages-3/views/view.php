
<?php var_dump($atts);?>
<section class="uk-container">
  <?php if ($atts['panel3_advantage_block_title']) {?>
  <div class="title">
    <?php echo $atts['panel3_advantage_block_title'];?>
  </div>
  <?php }?>

  <div class="uk-grid uk-child-width-1-2@s">
    <?php foreach ($atts['panel3_advantages'] as $item) {?>
      <div class="uk-margin-medium-bottom">
        <div class="fs18@s fs24@l fnt-bld uk-margin-small-bottom uk-margin-medium-bottom@m">
          <?php echo $item['panel3_advantage_title'];?>
        </div>
        <div class="fs16 fs18@m">
          <?php echo $item['panel3_advantage_desc'];?>
        </div>
      </div>
    <?php }?>
  </div>
</section>
