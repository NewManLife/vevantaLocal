<?php

$options = [
  'panel3_advantage_block_title'  => [
    'type' => 'text',
    'label' => 'Заголовок блока',
  ],
  'panel3_advantages' => array(
      'type'  => 'addable-box',
      'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
      'label' => __('Преимущество', '{domain}'),
      'box-options' => array(
        'panel3_advantage_title'  => [
          'type' => 'text',
          'label' => 'Заголовок',
        ],
        'panel3_advantage_desc' => array(
          'type' => 'textarea', 'label' => 'Описание',
        ),
      ),
      'template' => '{{- panel3_advantage_title }}', // box title

      'limit' => 0, // limit the number of boxes that can be added
      'add-button-text' => __('Добавить', '{domain}'),
      'sortable' => true,
  )
];
