<section class="uk-position-relative bunner">
  <div
  class=" uk-background-norepeat uk-background-center-right uk-width-1-1 uk-height-1-1 uk-positon-absolute uk-position-top-right"
  data-src="<?php echo $atts['panel_bunner_bg']['url'];?>"
  uk-img
  >
  </div>
  <div class="uk-container uk-height-1-1">
    <div class="uk-height-1-1 uk-position-relative">
      <div
        class="uk-width-1-2@m pr10 item__bunner uk-z-index uk-position-center-left"
        >
        <div class="fs22 fs26@s fs38@m fnt-bld">
          <?php echo $atts['title_bunner'];?>
        </div>
        <div class="fs20 fs24@s fs34@m uk-margin-small-top">
          <?php echo $atts['subtitle_bunner'];?>
        </div>
        <div class="uk-margin-medium-top">
          <form-phone
            <?php if ($atts['btn_title_bunner']) {?>
              button-name="<?php echo $atts['btn_title_bunner'];?>"
            <?php }?>
          ></form-phone>
        </div>
      </div>
    </div>
  </div>
</section>
