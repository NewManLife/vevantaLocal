<?php if (!defined('FW')) die('Forbidden');

// find the uri to the shortcode folder
$uri = fw_get_template_customizations_directory_uri('/extensions/shortcodes/shortcodes/bunner');

wp_enqueue_style('fw-shortcode-bunner-shortcode', $uri . '/static/css/styles.css');
