<?php
  $options = [
    'title_bunner'  => [
      'type' => 'text',
      'label' => 'Заголовок баннера',
      'max' => 2,
      'fw-storage' => [
        'type' => 'post-meta',
        'post-meta' => 'title_bunner',
      ],
    ],
    'subtitle_bunner'  => [
      'type' => 'text',
      'label' => 'Подзаголовок баннера',
      'max' => 2,
      'fw-storage' => [
        'type' => 'post-meta',
        'post-meta' => 'subtitle_bunner',
      ],
    ],
    'btn_title_bunner'  => [
      'type' => 'text',
      'label' => 'Название кнопки',
      'max' => 2,
      'fw-storage' => [
        'type' => 'post-meta',
        'post-meta' => 'btn_title_bunner',
      ],
    ],
    'panel_bunner_bg' => [
      'type'  => 'upload',
      'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
      'label' => __('Фон', '{domain}'),
      'images_only' => true,
      'files_ext' => array( 'webp', 'jpeg',  'jpg',  'png',  'svg'),
      'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
    ],
  ];
