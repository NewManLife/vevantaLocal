<?php

$options = [
  'panel_advantage_title'  => [
    'type' => 'text',
    'label' => 'Преимущества',
    'value' => 'Преимущество работы с нами',
    'max' => 2,
  ],
  'panel_advantage_items' => array(
      'type'  => 'addable-box',
      'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
      'label' => __('Преимущество', '{domain}'),
      'box-options' => array(
        'panel_advantage_item_title' => [
          'type' => 'text',
          'label' => 'Заголовок',
        ],
        'panel_advantage_button_color' => array(
            'type'  => 'switch',
            'value' => 'hello',
            'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
            'label' => __('Цвет', '{domain}'),
            'left-choice' => array(
                'value' => false,
                'label' => __('Тёмный', '{domain}'),
            ),
            'right-choice' => array(
                'value' => true,
                'label' => __('Светлый', '{domain}'),
            ),
        ),
        'panel_advantage_item_source' => [
          'type' => 'text',
          'label' => 'Ссылка',
          'desc' => 'Ссылка не является обязательной',
        ],
        'panel_advantage_img' => [
          'type'  => 'upload',
          'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
          'label' => __('Изображение', '{domain}'),
          'images_only' => true,
          'files_ext' => array( 'webp', 'jpeg',  'jpg',  'png',  'svg'),
          'desc' => 'Размер: 277x426',
          'extra_mime_types' => array( 'audio/x-aiff, aif aiff' ),
        ],
      ),
      'template' => '{{- panel_advantage_item_title }}', // box title
      'limit' => 0, // limit the number of boxes that can be added
      'add-button-text' => __('Добавить', '{domain}'),
      'sortable' => true,
  )
];
