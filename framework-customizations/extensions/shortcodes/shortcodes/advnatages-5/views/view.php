<?php if (!empty($atts['panel_advantage_items'])) {?>
<section class="uk-background-muted-default">
  <div class="uk-container">
    <div class="title"><?php echo $atts['panel_advantage_title'];?></div>
    <div uk-slider>
      <div class="uk-position-relative uk-light" tabindex="-1">
        <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-6@m uk-grid">
          <?php foreach ($atts['panel_advantage_items'] as $item) {?>
          <li>
            <?php if ($item['panel_advantage_item_source']) {?>
              <a target="blank" href="<?php echo $item['panel_advantage_item_source'];?>" class="uk-flex uk-position-relative show-animation" target="_blank">
                <img data-src="<?php echo $item['panel_advantage_img']['url'];?>" alt="" uk-img>
                <div class="fs14 fs16@s <?php if ($item['panel_advantage_button_color']) {echo 'cl-wh';} else {echo 'cl-dark';}?> uk-position-top-left uk-padding-small">
                  <?php echo $item['panel_advantage_item_title'];?>
                </div>
              </a>
            <?php } else {?>
              <div class="uk-flex uk-position-relative show-animation" target="_blank">
                <img data-src="<?php echo $item['panel_advantage_img']['url'];?>" alt="" uk-img>
                <div class="fs14 fs16@s <?php if ($item['panel_advantage_button_color']) {echo 'cl-wh';} else {echo 'cl-dark';}?> uk-position-top-left uk-padding-small">
                  <?php echo $item['panel_advantage_item_title'];?>
                </div>
              </div>
            <?php } ?>


          </li>
        <?php }?>
        </ul>
        <div class="slider__route">
          <a class="uk-position-center-left" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
          <a class="uk-position-center-right" href="#"  uk-slidenav-next uk-slider-item="next"></a>
        </div>
      </div>
      <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
    </div>
  </div>
</section>
<?php } ?>
