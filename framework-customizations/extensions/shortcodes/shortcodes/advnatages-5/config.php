<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Преимущества - 5', 'fw' ),
	'description' => __( 'Преимущества - 5', 'fw' ),
	'tab'         => __( 'Преимущества', 'forms' ),
  'popup_size'    => 'medium', // can be large, medium or small
];
