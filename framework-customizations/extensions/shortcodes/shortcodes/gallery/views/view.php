<div class="uk-grid uk-child-width-1-3@l uk-child-width-1-2" uk-lightbox>
  <?php foreach ($atts['sk_images'] as $img): ?>
    <a href="<?php echo wp_get_attachment_image_src($img['attachment_id'], 'full')[0];?>">
      <div class="uk-height-medium@s uk-height-xsmall uk-width-1-1 uk-position-relative uk-visible-toggle cr-pointer uk-background-cover uk-margin-bottom"
      data-src="<?php echo wp_get_attachment_image_src($img['attachment_id'], 'medium')[0];?>" uk-img>
        <div class="uk-position-bottom uk-padding-small uk-invisible-hover uk-animation-fade">
          <?php if (wp_get_attachment_caption( $img['attachment_id'] )) {?>
          <div class="bg-wblue uk-width-auto uk-padding-small fnt-med uk-border-rounded fs16@s fs12">
            <?php echo wp_get_attachment_caption( $img['attachment_id'] );?>
          </div>
          <?php }?>
        </div>
      </div>
    </a>
  <?php endforeach; ?>
</div>
