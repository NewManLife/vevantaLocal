<?php
  $ids = explode(';', $atts['panel_news_ids']);
?>
<section class="uk-container uk-margin-medium-bottom">
  <div class="uk-child-width-1-4@m uk-child-width-1-2@s uk-grid-small" uk-grid>
    <?php foreach ($ids as $id) {
      $post = get_post($id);
    ?>
    <a class="uk-grid uk-grid-collapse uk-margin-small-bottom uk-margin-small-top show-animation" href="<?php echo get_permalink($post->ID);?>" target="_blank">
      <div class="uk-height-small uk-background-cover uk-width-1-2 uk-width-1-1@s uk-margin-small-right uk-margin-remove-right@s" data-src="<?php echo get_the_post_thumbnail_url($post->ID, 'medium')?>" uk-img>
      </div>
      <div class="fnt-med cl-dark uk-margin-small-top uk-width-expand fs14 fs16@s">
        <?php echo $post->post_title;?>
      </div>
    </a>
    <hr class="uk-hidden@s uk-margin-remove">
    <?php }?>
  </div>
<hr class="uk-visible@s uk-margin-medium">
</section>
