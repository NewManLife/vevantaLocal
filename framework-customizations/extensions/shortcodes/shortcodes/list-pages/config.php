<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Страницы блоками', 'fw' ),
	'description' => __( 'Страницы блоками', 'fw' ),
	'tab'         => __( 'Преимуществ', 'forms' ),
];
