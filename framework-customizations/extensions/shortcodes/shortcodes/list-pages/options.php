<?php

$options = [
  'panel_news_ids'  => [
    'type' => 'text',
    'label' => 'ID страниц ',
    'desc' => 'Пример: 258;235',
    'help' => 'ID можно взять в URL страницы <img src="'. get_template_directory_uri().'/framework-customizations/extensions/shortcodes/shortcodes/list-pages/static/img/help.png" style="max-width: 100%;">'
  ]
];
