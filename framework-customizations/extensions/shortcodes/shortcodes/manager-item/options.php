<?php

$options = [
  'title'   => [
      'label'   => __('Заголовок карточки', '{domain}'),
      'type'    => 'text',
      'value' => 'Хотите купить со скидкой?',
  ],
  'avatar'   => [
      'label'   => __('Аватар специалиста', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],

  'name'   => [
      'label'   => __('Должность специалиста', '{domain}'),
      'type'    => 'text',
      'value' => 'Специалист по недвижимости',
  ],

  'position' => [
    'type'  => 'radio',
    'value' => 'center',
    'attr'  => ['class' => 'custom-class', 'data-foo' => 'bar'],
    'label' => __('Расположение формы', '{domain}'),
    'choices' => [
        'left' => __('слево', '{domain}'),
        'right' => __('Справа', '{domain}'),
        'center' => __('По центру', '{domain}'),
    ],
    'inline' => true,
  ],

  'phone'   => [
      'label'   => __('Номер телефона специалиста', '{domain}'),
      'type'    => 'text',
      'value' => '3452692347',
  ],
  'class_id'   => [
      'label'   => __('Class объекта(Тип недвижимости)', '{domain}'),
      'type'    => 'text',
      'value' => false,
  ],
  'staff_id'   => [
      'label'   => __('ID ответственного', '{domain}'),
      'type'    => 'text',
      'help' => '48 по умолчанию Марина Михно',
      'value' => 48,
  ],
  'manager_id'   => [
      'label'   => __('ID менеджера', '{domain}'),
      'type'    => 'text',
      'help' => '0 - Отключен',
      'value' => 0,
  ],
];
