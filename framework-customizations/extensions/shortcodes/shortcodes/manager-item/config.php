<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Карточка специалист по недвижимости', 'fw' ),
	'description' => __( 'Компонент карточка специалист по недвижимости + номер + кнопка', 'fw' ),
];
