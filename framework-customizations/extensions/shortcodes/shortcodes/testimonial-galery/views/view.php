<section class="uk-container">
  <div class="fs18 fs32@s fnt-bld uk-margin-top">
      <?=$atts['title'];?>
  </div>
  <div class="uk-margin-top uk-position-relative" tabindex="-1" uk-slider>


    <div class="uk-slider-container">
    <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-2@m uk-grid">
      <?php foreach ($atts['cards'] as $key=>$card): ?>
        <li>
          <div class="uk-panel review">
            <div>
              <div class="uk-grid-small" uk-grid >
                <div class="uk-grid-small uk-flex-middle" uk-grid>
                    <div class="uk-width-auto">
                        <img class="uk-border-circle" width="70" height="70" data-src="<?=$card['cards_avatar']['url'];?>" alt="" uk-img>
                    </div>
                    <div class="uk-width-expand">
                      <p><span class="fs16 fs18@s uk-text-bold uk-margin-remove-bottom"><?=$card['cards_title'];?></span>
                        <br><i class="uk-text-muted uk-margin-remove-top"><?=$card['cards_subtitle'];?></i>
                      </p>
                    </div>
                </div>
                <div class="uk-width-1-1">
                  <div id="text-<?=$key?>" class="uk-link uk-link-toggle" style="height: 100px;overflow: hidden;" onclick="this.style.height = 'auto'; document.getElementById('block-<?=$key ?>').style.display='none'">
                    <div><?=$card['cards_text'];?></div>
                  </div>
                  <div id="block-<?=$key?>" style="cursor: pointer;" class="linkText uk-padding-remove uk-margin-remove uk-text-right" onclick="document.getElementById('text-<?=$key ?>').style.height = 'auto'; this.style.display = 'none';">
                    <div style="color: #00bab6;"><?=$card['cards_text_link'];?></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      <?php endforeach;?>
    </ul>

    <div class="uk-hidden@s uk-light">
        <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
        <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
    </div>

    <div class="uk-visible@s">
        <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
        <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
    </div>

    </div>


  </div>
</section>

