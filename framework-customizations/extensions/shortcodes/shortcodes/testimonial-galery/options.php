<?php
$options = [
  'title'   => [
      'label'   => __('Заголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Истории успеха',
  ],
  'cards' => array(
    'type' => 'addable-popup',
    'label' => __('Карточка', '{domain}'),
    'template' => '{{- cards_title }}',
    'popup-title' => null,
    'size' => 'small', // small, medium, large
    'limit' => 0, // limit the number of popup`s that can be added
    'add-button-text' => __('Добавить', '{domain}'),
    'sortable' => true,
    'fw-storage' => [
      'type' => 'post-meta',
      'post-meta' => 'comments',
    ],
    'popup-options' => array(
        'cards_avatar' => array(
          'type'  => 'upload',
          'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
          'label' => __('Картинка 60х60', '{domain}'),
          'images_only' => true,
          'files_ext' => array( 'png', 'jpg', 'jpeg' ),
          'extra_mime_types' => array( 'audio/x-aiff, aif aiff' )
      ),
        'cards_title' => array(
            'label' => __('Заголовок', '{domain}'),
            'type' => 'text',
            'value' => 'Евгений Казаков',
        ),
        'cards_subtitle' => array(
            'label' => __('Подзаголовок', '{domain}'),
            'type' => 'text',
            'value' => 'Менеджер группы',
        ),
        'cards_text' => array(
            'label' => __('Текст карточки', '{domain}'),
            'type' => 'textarea',
            'value' => 'В Тюменском Центре недвижимости Веванта я работаю со дня открытия, сначала был специалистом по недвижимости, параллельно прошел обучение и подготовку в качестве менеджера группы риелторов. Показал свою необходимость для компании, выполнял плановые показатели, продемонстрировал лидерские качества и профессиональные навыки. Компания высоко оценила мои старания в финансовом плане и в карьерном росте. Моя цель – работать и развиваться дальше, вести за собой коллектив молодых специалистов.',
            ),
          'cards_text_link' => array(
            'label' => __('Текст ссылки', '{domain}'),
            'type' => 'text',
            'value' => 'Читать далее...',
            ),
        ),
    ),
];
