<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Мини карточки вывод квартир из базы', 'fw' ),
	'description' => __( 'Компонент для вывода квартир из базы мини карточками', 'fw' ),
];
