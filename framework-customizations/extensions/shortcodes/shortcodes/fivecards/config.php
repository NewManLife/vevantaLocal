<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'Карточки без кнопки', 'fw' ),
	'description' => __( 'Пять карточек без кнопки с настраиваемыми иконками', 'fw' ),
];
