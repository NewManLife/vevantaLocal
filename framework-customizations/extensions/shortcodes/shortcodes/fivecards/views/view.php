<?php
  $items = array();
  for($i = 0; $i < count($atts['items']); $i++) {
    array_push($items, $atts['items'][$i]);
  }
?>
<Five-Cards

  :info="<?php echo htmlspecialchars(json_encode($items), ENT_QUOTES, 'UTF-8'); ?>"

></Five-Cards>
