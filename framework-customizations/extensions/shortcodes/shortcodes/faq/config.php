<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = [];

$cfg['page_builder'] =[
	'title'       => __( 'FAQ', 'fw' ),
	'description' => __( 'FAQ', 'fw' ),
	'tab'         => __( 'Преимуществ', 'forms' ),
];
