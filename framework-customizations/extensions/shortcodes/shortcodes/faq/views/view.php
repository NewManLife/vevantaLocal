<section class="uk-container uk-margin-medium-bottom">
  <?php if ($atts['panel_faq_block_title']) {?>
    <div class="title uk-margin-remove-top"><?php echo $atts['panel_faq_block_title'];?></div>
  <?php }?>

  <ul uk-accordion>
    <?php foreach ($atts['panel_faq_questions'] as $item): ?>
      <li>
        <a class="uk-accordion-title fnt-med" href="#">
          <span class="fs16 fs18@m"><?php echo $item['panel_faq_questions_title'];?></span>
        </a>
        <div class="uk-accordion-content fs14 fs16@s">
          <p>
            <?php echo $item['panel_faq_questions_desc'];?>
          </p>
        </div>
      </li>
    <?php endforeach; ?>
  </ul>
</section>
