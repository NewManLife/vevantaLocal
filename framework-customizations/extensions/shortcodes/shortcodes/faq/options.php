<?php

$options = [
  'panel_faq_block_title'  => [
    'type' => 'text',
    'label' => 'Название блока',
    'value' => 'Ответы на главные вопросы',
    'help' => 'По умолчанию: Ответы на главные вопросы',
  ],
  'panel_faq_questions' => array(
      'type'  => 'addable-box',
      'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
      'label' => __('Вопрос', '{domain}'),
      'box-options' => array(
        'panel_faq_questions_title' => [
          'type' => 'text',
          'label' => 'Вопрос',
          'max' => 2,
        ],
        'panel_faq_questions_desc' => array(
          'type' => 'textarea', 'label' => 'Ответ',

        ),
      ),
      'template' => '{{- panel_faq_questions_title }}', // box title
      'limit' => 0, // limit the number of boxes that can be added
      'add-button-text' => __('Добавить', '{domain}'),
      'sortable' => true,
  )
];
