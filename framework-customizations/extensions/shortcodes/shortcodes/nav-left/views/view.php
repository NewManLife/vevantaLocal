<Nav-Left

title="<?=$atts['title']?>"
img="<?php echo $atts['img']?>"

class-title="<?=$atts['classtitle']?>"
class-title-nav="<?php echo $atts['classtitlenav']?>"
class-title-text="<?php echo $atts['classtitletext']?>"
class-subtitle-text="<?php echo $atts['classsubtitletext']?>"
class-message="<?php echo $atts['classmessage']?>"
class-button-text="<?php echo $atts['classbuttontext']?>"


cards="<?php echo htmlspecialchars(json_encode($atts['cards']), ENT_QUOTES, 'UTF-8'); ?>"


></Nav-Left>
