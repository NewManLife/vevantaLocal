<?php
$options = [
    'title'   => [
      'label'   => __('Заголовок формы', '{domain}'),
      'type'    => 'text',
      'value' => 'Вакансии',
  ],
    'img'   => [
      'label'   => __('Картинка-фон формы', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
    'cards' => array(
        'type' => 'addable-popup',
        'label' => __('Элемент меню', '{domain}'),
        'popup-title' => null,
        'size' => 'small', // small, medium, large
        'template' => '{{- titleNav }}',
        'limit' => 0, // limit the number of popup`s that can be added
        'add-button-text' => __('Добавить', '{domain}'),
        'sortable' => true,
        'fw-storage' => [
        'type' => 'post-meta',
        'post-meta' => 'comments',
        ],
        'popup-options' => array(
                'titleNav' => array(
                    'label' => __('Заголовок меню', '{domain}'),
                    'type' => 'text',
                    'value' => 'Риелтор / Специалист по недвижимости',
                ),
                'titleText' => array(
                    'label' => __('Заголовок текста', '{domain}'),
                    'type' => 'text',
                    'value' => 'Зарплата до 120 000 руб',
                ),
                'text' => array(
                    'type' => 'addable-popup',
                    'label' => __('Подзаголовок', '{domain}'),
                    'popup-title' => null,
                    'size' => 'small', // small, medium, large
                    'template' => '{{- subtitleText }}',
                    'limit' => 0, // limit the number of popup`s that can be added
                    'add-button-text' => __('Добавить', '{domain}'),
                    'sortable' => true,
                    'fw-storage' => [
                    'type' => 'post-meta',
                    'post-meta' => 'comments',
                    ],
                    'popup-options' => array(
                            'subtitleText' => array(
                                'label' => __('Подзаголовок текста ', '{domain}'),
                                'type' => 'text',
                                'value' => 'Основные задачи:',
                            ),
                            'textItem' => array(
                                'type' => 'addable-popup',
                                'label' => __('Подзаголовок', '{domain}'),
                                'popup-title' => null,
                                'size' => 'small', // small, medium, large
                                'template' => '{{- item }}',
                                'limit' => 0, // limit the number of popup`s that can be added
                                'add-button-text' => __('Добавить', '{domain}'),
                                'sortable' => true,
                                'fw-storage' => [
                                'type' => 'post-meta',
                                'post-meta' => 'comments',
                                ],
                                'popup-options' => array(
                                        'item' => array(
                                            'label' => __('Подзаголовок текста ', '{domain}'),
                                            'type' => 'text',
                                            'value' => '- Стабильный входящий поток от компании;',
                                        ),
                                    ),
                                ),
                        ),
                    ),
                'buttonText' => array(
                    'label' => __('текст кнопки после текста', '{domain}'),
                    'type' => 'text',
                    'value' => 'Отправить резюме',
                ),
                'targetButtonText' => array(
                    'label' => __('Таргет кнопки на модальное окна', '{domain}'),
                    'type' => 'text',
                    'value' => 'target: #modal-worksAdv',
                ),
                'urlButtonText' => array(
                    'label' => __('Url куда ведет кнопка', '{domain}'),
                    'type' => 'text',
                    'value' => '',
                ),
            ),
        ),
    'classTitle'   => [
      'label'   => __('Класс стиля заголовка', '{domain}'),
      'type'    => 'text',
      'value' => 'fs18 fs20@s fs24@m fs28@l fnt-med',
  ],
    'classTitleNav'   => [
      'label'   => __('Класс стиля меню слева', '{domain}'),
      'type'    => 'text',
      'value' => 'fs16 fs20@m uk-padding-small fnt-bld',
  ],
    'classTitleText'   => [
      'label'   => __('Класс стиля основного заголовка справа', '{domain}'),
      'type'    => 'text',
      'value' => 'fs18 fs20@s fs22@l fnt-bld uk-margin-bottom',
  ],
    'classSubtitleText'   => [
      'label'   => __('Класс стиля заголовка раздела  справа', '{domain}'),
      'type'    => 'text',
      'value' => 'fs18 fs20@s fs22@l fnt-bld',
  ],
    'classMessage'   => [
      'label'   => __('Класс стиля текста раздела  справа', '{domain}'),
      'type'    => 'text',
      'value' => 'fs16 fs18@l',
  ],
    'classButtonText'   => [
      'label'   => __('Класс стиля кнопки', '{domain}'),
      'type'    => 'text',
      'value' => 'btnText uk-button uk-button-default fs16 fs18@l',
  ],

];
