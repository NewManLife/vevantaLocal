<Hero-Bottom

  title="<?=$atts['title']?>"

  subtitle="<?=$atts['subtitle']?>"

  img="<?php echo $atts['img']?>"

  img-upload="<?php echo $atts['imgupload']?>"

  img-rigth="<?php echo $atts['imgrigth']?>"

  text-upload="<?=$atts['textupload']?>"

  button-text="<?=$atts['buttontext']?>"

  style-text="<?=$atts['styletext']?>"

  style-text-upl="<?=$atts['styletextupl']?>"

  style-politika="<?=$atts['stylepolitika']?>"

  style-politika-input="<?=$atts['stylepolitikainput']?>"

  style-politika-link="<?=$atts['stylepolitikalink']?>"

  staff-id="<?=$atts['staffid']?>"

  manager-id="<?=$atts['managerid']?>"

  class-id="<?=$atts['classid']?>"
  
  source-id="<?=$atts['sourceid']?>"

></Hero-Bottom>
