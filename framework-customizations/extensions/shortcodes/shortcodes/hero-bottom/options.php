<?php

$options = [
  'title'   => [
      'label'   => __('Заголовок', '{domain}'),
      'type'    => 'text',
      'value' => 'Нет подходящей вакансии?',
  ],

  'subtitle'   => [
      'label'   => __('Подзаголовок', '{domain}'),
      'type'    => 'text',
      'value' => 'Отправь нам свое резюме, мы формируем кадровый резерв. Возможно, завтра нам понадобишься именно ты!',
  ],

  'img'   => [
      'label'   => __('Url картинки фона формы', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],

  'imgUpload'   => [
      'label'   => __('Url картинки отправки документа 15х15', '{domain}'),
      'type'    => 'text',
      'value' => 'https://vevanta.ru/wp-content/uploads/2021/02/clip.png',
  ],

  'imgRigth'   => [
      'label'   => __('Url картинки справа от формы', '{domain}'),
      'type'    => 'text',
      'value' => 'https://vevanta.ru/wp-content/uploads/2021/02/glass.png',
  ],

  'textUpload'   => [
      'label'   => __('Текст отправки документа', '{domain}'),
      'type'    => 'text',
      'value' => 'Прикрепить резюме',
  ],

  'buttonText'   => [
      'label'   => __('Текст кнопки', '{domain}'),
      'type'    => 'text',
      'value' => 'Отправить резюме',
  ],

  'styleText'   => [
      'label'   => __('Стиль текста', '{domain}'),
      'type'    => 'text',
      'value' => 'color: #000;',
  ],

  'styleTextUpl'   => [
      'label'   => __('Стиль текста загрузки', '{domain}'),
      'type'    => 'text',
      'value' => 'color: #8f8484;',
  ],

  'stylePolitika'   => [
      'label'   => __('Стиль текста политики', '{domain}'),
      'type'    => 'text',
      'value' => 'color: #000; border: none;',
  ],

  'stylePolitikaInput'   => [
      'label'   => __('Стиль chekbox политики', '{domain}'),
      'type'    => 'text',
      'value' => 'background-color: #00bab6;border: none;',
  ],

  'stylePolitikaLink'   => [
      'label'   => __('Стиль ссылки политики', '{domain}'),
      'type'    => 'text',
      'value' => 'color: #00bab6; border: none;',
  ],
  'staffId'   => [
      'label'   => __('staffId', '{domain}'),
      'type'    => 'text',
      'value' => '110',
  ],
    'managerId'   => [
      'label'   => __('managerId', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
    'classId'   => [
      'label'   => __('classId', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
    'sourceId'   => [
      'label'   => __('sourceId', '{domain}'),
      'type'    => 'text',
      'value' => '',
  ],
  

];
