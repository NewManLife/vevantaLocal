<section>
  <div class="uk-container">
    <div class="title uk-text-center">
      Отзывы
    </div>
    <div uk-slider>
      <div class="uk-position-relative" tabindex="-1">

        <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-3@l  uk-child-width-1-2@s uk-grid uk-text-center">
          <?php foreach ($atts['comments'] as $comment): ?>
            <li>
              <div class="uk-width-1-1">
                <img class="uk-border-circle" data-src="<?php echo $comment['comment_avatar']['url'];?>" alt="" uk-img>
                <div class="fnt-med uk-margin-small-top"><?php echo $comment['comment_name'];?></div>
                <div class="bg-wblue uk-padding-small uk-border-rounded uk-margin-top fnt-bold">
                  <?php echo $comment['comment_desc'];?>
                </div>
              </div>
            </li>
          <?php endforeach;?>
        </ul>
        <div class="slider__route">
          <a class="uk-position-center-left" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
          <a class="uk-position-center-right" href="#"  uk-slidenav-next uk-slider-item="next"></a>
        </div>
      </div>
    </div>
  </div>
</section>
