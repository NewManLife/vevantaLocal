<?php
$options = [
  'comments' => array(
    'type' => 'addable-popup',
    'label' => __('Комментарии', '{domain}'),
    'template' => '{{- comment_name }}',
    'popup-title' => null,
    'size' => 'small', // small, medium, large
    'limit' => 0, // limit the number of popup`s that can be added
    'add-button-text' => __('Добавить', '{domain}'),
    'sortable' => true,
    'fw-storage' => [
      'type' => 'post-meta',
      'post-meta' => 'comments',
    ],
    'popup-options' => array(
        'comment_avatar' => array(
          'type'  => 'upload',
          'attr'  => array( 'class' => 'custom-class', 'data-foo' => 'bar' ),
          'label' => __('Аватар', '{domain}'),
          'images_only' => true,
          'files_ext' => array( 'png', 'jpg', 'jpeg' ),
          'extra_mime_types' => array( 'audio/x-aiff, aif aiff' )
      ),
        'comment_name' => array(
            'label' => __('Имя', '{domain}'),
            'type' => 'text',
            'value' => '',
        ),
        'comment_desc' => array(
            'label' => __('Комментарий', '{domain}'),
            'type' => 'textarea',
            'value' => '',
            ),
        ),
    ),
];
