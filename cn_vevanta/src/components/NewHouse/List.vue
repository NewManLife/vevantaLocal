<template>
  <div class="list-container">
    <section
      class="uk-section uk-background-center"
      data-src="https://vevanta.ru/wp-content/themes/agentstvo/assets/images/home/bunner.jpg"
      uk-img
      v-if="!isMobile"
    >
      <div class="uk-container filter-container">
        <template-filter
          :filter="filter"
          :isShow="isFilterShow"
          :microdistricts="microdistricts"
          :developers="developers"
          :complexes="complexes"
          :rooms="rooms"
          :walls="walls"
          :statuses="statuses"
          @get-objects="getObjects"
          @hideFilter="isFilterShow = false"
        ></template-filter>
        <p
          class="uk-padding-small uk-margin-remove uk-padding-remove-bottom uk-padding-remove-top"
        >
          <span
            class="uk-badge filter-item"
            v-for="item in getFilterItems"
            :key="item.id"
            @click="delFilter(item.id)"
            >{{ item.title }}
            <span uk-icon="icon: close; ratio: .7"></span>
          </span>
        </p>
      </div>
    </section>

    <div v-else class="uk-margin-medium-top">
      <template-filter
        :filter="filter"
        :isShow="isFilterShow"
        :microdistricts="microdistricts"
        :complexes="complexes"
        :developers="developers"
        :rooms="rooms"
        :walls="walls"
        :statuses="statuses"
        @get-objects="getObjects"
        @hideFilter="isFilterShow = false"
      ></template-filter>

      <div
        class="uk-padding-small uk-padding-remove-top uk-padding-remove-bottom"
      >
        <button
          class="uk-button btn btn__tpl2 uk-width-1-1 uk-margin-small-bottom"
          @click="isFilterShow = !isFilterShow"
          v-if="!isFilterShow"
        >
          Открыть фильтр
        </button>
      </div>
      <p class="uk-margin-remove uk-padding-small uk-padding-remove-top">
        <span
          class="uk-badge filter-item"
          v-for="item in getFilterItems"
          :key="item.id"
          @click="delFilter(item.id)"
          >{{ item.title }}
          <span uk-icon="icon: close; ratio: .7"></span>
        </span>
      </p>
    </div>

    <div
      class="uk-container uk-padding-remove uk-margin-small"
      ref="objects_list"
    >
      <div
        class="uk-padding-small uk-padding-remove-top uk-padding-remove-bottom"
      >
        <h1 class="title">Новостройки в Тюмени</h1>
        <p class="uk-margin">
          Найдено
          {{ count | declination("предложение", "предложения", "предложений") }}
        </p>
        <div class="uk-flex uk-flex-between uk-margin">
          <div>
            <button
              class="uk-button uk-button-default toggle-button"
              :class="{ active: viewType == 'tile' }"
              @click="viewType = 'tile'"
            >
              <span uk-icon="icon: thumbnails; ratio: 1"></span>
            </button>
            <button
              class="uk-button uk-button-default toggle-button"
              :class="{ active: viewType == 'list' }"
              @click="viewType = 'list'"
            >
              <span uk-icon="icon: menu; ratio: 1"></span>
            </button>
          </div>

          <div class="uk-flex uk-flex-right uk-hidden@s">
            <div class="uk-form-controls uk-flex uk-flex-right">
              <button
                class="uk-button uk-button-default uk-background-default"
                type="button"
                style="padding: 0px 10px;"
              >
                {{ sortType.value }}
                <span
                  :uk-icon="`icon: arrow-${sortType.key.split('-')[1]}`"
                ></span>
              </button>
              <div uk-dropdown="pos: bottom-left" class="uk-padding-small">
                <ul
                  class="uk-list uk-margin-remove"
                  uk-accordion="collapsible: true"
                >
                  <li>
                    <p @click="selectSortType('price')">
                      По цене
                      <span
                        v-if="sortType.key.indexOf('price') != -1"
                        :uk-icon="`icon: arrow-${sortType.key.split('-')[1]}`"
                      ></span>
                    </p>
                  </li>
                  <li>
                    <p @click="selectSortType('square')">
                      По площади
                      <span
                        v-if="sortType.key.indexOf('square') != -1"
                        :uk-icon="`icon: arrow-${sortType.key.split('-')[1]}`"
                      ></span>
                    </p>
                  </li>
                  <li>
                    <p @click="selectSortType('year')">
                      По дате
                      <span
                        v-if="sortType.key.indexOf('year') != -1"
                        :uk-icon="`icon: arrow-${sortType.key.split('-')[1]}`"
                      ></span>
                    </p>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <template v-if="count == 0">
        <h4 class="uk-padding-small">
          По вашему запросу ничего не найдено. Попробуйте изменить параметры
          запроса
        </h4>
      </template>

      <template v-else>
        <div class="uk-padding-small uk-padding-remove-top uk-padding-remove-bottom uk-margin-small-top uk-flex uk-flex-between">
          <div>
            <pagination
              @changePage="changePage"
              :count="count"
              :limit="limit"
              :page="page"
              :position="'left'"
            />
          </div>
          <div>
            <div
              class="uk-width-expand uk-flex uk-flex-right uk-visible@s uk-margin-remove"
              uk-grid
            >
              <div class="uk-card sort-button uk-padding-small-left">
                <p @click="selectSortType('price')">
                  По цене
                  <span
                    v-if="sortType.key.indexOf('price') != -1"
                    :uk-icon="`icon: arrow-${sortType.key.split('-')[1]}`"
                  ></span>
                  <span v-else class="invisible"></span>
                </p>
              </div>

              <div class="uk-card sort-button uk-padding-small-left">
                <p @click="selectSortType('square')">
                  По площади
                  <span
                    v-if="sortType.key.indexOf('square') != -1"
                    :uk-icon="`icon: arrow-${sortType.key.split('-')[1]}`"
                  ></span>
                  <span v-else class="invisible"></span>
                </p>
              </div>

              <div class="uk-card sort-button uk-padding-small-left">
                <p @click="selectSortType('year')">
                  По дате
                  <span
                    v-if="sortType.key.indexOf('year') != -1"
                    :uk-icon="`icon: arrow-${sortType.key.split('-')[1]}`"
                  ></span>
                  <span v-else class="invisible"></span>
                </p>
              </div>

            </div>
          </div>
        </div>

        <component
          class="uk-margin uk-padding-small uk-padding-remove-top uk-padding-remove-bottom"
          :is="currentViewType"
          :objects="sortList.slice(0, limit)"
        ></component>

        <pagination
          @changePage="changePage"
          :count="count"
          :limit="limit"
          :page="page"
          :position="'center'"
        />
      </template>
    </div>
  </div>
</template>

<script>
import listView from "./ListView";
import tileView from "./TileView";
import templateFilter from "./Filter";
import Pagination from "./Pagination";

import input from "../../filters/Input";

export default {
  mixins: [input],
  props: {
    complex_id: {
      type: Number,
      default: null
    }
  },
  data() {
    return {
      isFilterShow: true,
      viewType: "list",
      sortType: {
        key: "price-down",
        value: "По цене"
      },
      objects: [],
      microdistricts: [],
      complexes: [],
      developers: [],
      walls: [],
      statuses: [
        { id: 1, name: "Строится" },
        { id: 2, name: "Сдан/Сдается" }
      ],
      rooms: [
        { id: 1, name: "1 комнатная" },
        { id: 2, name: "2 комнатная" },
        { id: 3, name: "3 комнатная" },
        { id: 4, name: "4 комнатная" }
      ],
      count: 0,
      limit: 0,
      page: 1,
      offset: 1,
      filter: {
        microdistricts: [],
        comp_ids: [],
        dev_ids: [],
        rooms: [],
        walls: [],
        built_year: null,
        built_quartal: null,
        floors_from: null,
        floors_to: null,
        statuses: [],
        price_from: null,
        price_to: null,
        square_from: null,
        square_to: null
      }
    };
  },
  mounted() {
    this.getObjects(1);
    this.$axios
      .post(`/newhouse/getbuildingsinfo/${this.complex_id}`, {
        complex_id: this.complex_id
      })
      .then(res => {
        this.microdistricts = res.data.microdistricts;
        this.complexes = res.data.complexes;
        this.developers = res.data.developers;
        this.walls = res.data.walls;
      });

    if (this.isMobile) this.isFilterShow = false;
  },
  computed: {
    getFilterItems() {
      let filterItems = [];
      for (let i in this.filter) {
        if (!!!this.filter[i] || this.filter[i].length == 0) continue;

        let val = this.filter[i];
        switch (i) {
          case "statuses":
            if (val.length == 0) break;
            val.forEach(v => {
              let status = this.statuses.find(status => status.id == v).name;
              filterItems.push({
                id: `${i}-${v}`,
                title: `Статус: ${status}`
              });
            });
            break;
          case "microdistricts":
            if (val.length == 0) break;
            val.forEach(v => {
              let districtName = this.microdistricts.find(
                district => district.id == v
              ).name;
              filterItems.push({
                id: `${i}-${v}`,
                title: `Район: ${districtName}`
              });
            });
            break;
          case "dev_ids":
            if (val.length == 0) break;
            val.forEach(v => {
              let developerName = this.developers.find(
                developer => developer.id == v
              ).name;
              filterItems.push({
                id: `${i}-${v}`,
                title: `Застройщик: ${developerName}`
              });
            });
            break;
          case "comp_ids":
            if (val.length == 0) break;
            val.forEach(v => {
              let complexName = this.complexes.find(complex => complex.id == v)
                .name;
              filterItems.push({
                id: `${i}-${v}`,
                title: `ЖК: ${complexName}`
              });
            });
            break;
          case "rooms":
            if (val.length == 0) break;
            val
              .sort((a, b) => a - b)
              .forEach(v => {
                let rooms = this.rooms.find(room => room.id == v).name;
                filterItems.push({
                  id: `${i}-${v}`,
                  title: `${rooms.match(/\d/g)} к.`
                });
              });
            break;
          case "walls":
            if (val.length == 0) break;
            val.forEach(v => {
              let walls = this.walls.find(
                wall => wall.id == v
              ).name;
              filterItems.push({
                id: `${i}-${v}`,
                title: `Материал стен: ${walls}`
              });
            });
            break;
          case "price_from":
            filterItems.push({
              id: i,
              title: `Цена от: ${this.getSumStr(val)}`
            });
            break;
          case "price_to":
            filterItems.push({
              id: i,
              title: `Цена до: ${this.getSumStr(val)}`
            });
            break;
          case "square_from":
            filterItems.push({ id: i, title: `Площадь от: ${val} м²` });
            break;
          case "square_to":
            filterItems.push({ id: i, title: `Площадь до: ${val} м²` });
            break;
          case "floors_from":
            filterItems.push({ id: i, title: `Этажность от: ${val}` });
            break;
          case "floors_to":
            filterItems.push({ id: i, title: `Этажность до: ${val}` });
            break;
          case "built_year":
            filterItems.push({
              id: i,
              title: `Сдача: ${this.filter.built_year}-${this.filter.built_quartal}`
            });
            break;
          default:
            break;
        }
      }
      if (filterItems.length > 0) {
        filterItems.push({ id: "clear_all", title: "Отчистить все" });
      }
      return filterItems;
    },
    currentViewType() {
      return `${this.viewType}-view`;
    },
    sortList() {
      let [type, dir] = this.sortType.key.split("-");
      dir == "down" ? (dir = -1) : (dir = 1);
      if (type == "price") {
        return this.objects.sort((a, b) => (a.price_min - b.price_min) * dir);
      } else if (type == "square") {
        return this.objects.sort((a, b) => (a.square_min - b.square_min) * dir);
      } else if (type == "year") {
        return this.objects.sort((a, b) => {
          if (a.nh_built_year == b.nh_built_year)
            return (a.nh_ready_quarter - b.nh_ready_quarter) * dir;
          else return (a.nh_built_year - b.nh_built_year) * dir;
        });
      }
    },
    isMobile() {
      if (
        /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(
          navigator.userAgent
        )
      ) {
        return true;
      }
      return false;
    }
  },
  methods: {
    getObjects(offset = 1) {
      this.$axios
        .post("/newhouse", {
          offset,
          filter: this.filter
        })
        .then(res => {
          this.objects = res.data.items;
          this.limit = res.data.limit;
          this.page = res.data.page;
          this.count = res.data.count;
        });
    },
    getSumStr(str) {
      if (!!!str) return null;
      let res = str;
      if (str.length > 3 && str.length <= 6) {
        let dot = Number(str) % 1000 == 0 ? 0 : 1;
        res = `${(Number(str) / 1000).toFixed(dot)} тыс.`;
      } else if (str.length > 6) {
        let dot = Number(str) % 1000000 == 0 ? 0 : 1;
        res = `${(Number(str) / 1000000).toFixed(dot)} млн.`;
      }
      res += " ₽";
      return res;
    },

    delFilter(id) {
      if (id == "clear_all") {
        for (let i in this.filter) {
          if (typeof this.filter[i] == "object") this.filter[i] = [];
          else this.filter[i] = null;
        }
        return;
      }
      if (id.split("-").length == 2) {
        let [key, val] = id.split("-");
        let index = this.filter[key].indexOf(Number(val));
        this.filter[key].splice(index, 1);
      } else {
        this.filter[id] = null;
      }
    },
    changePage({ value }) {
      let [dir, val] = value.split("-");
      if (dir == "prev") this.page -= 1;
      if (dir == "next") this.page += 1;
      if (dir == "to") this.page = Number(val);
      this.getObjects(this.page);
      let offset = this.$refs.objects_list.offsetTop;
      window.scrollTo(0, offset);
    },
    selectSortType(type) {
      if (type == "year") this.sortType.value = "По дате";
      else if (type == "price") this.sortType.value = "По цене";
      else if (type == "square") this.sortType.value = "По площади";

      if (this.sortType.key.indexOf(type) == -1) {
        this.sortType.key = `${type}-down`;
      } else {
        if (this.sortType.key.indexOf("down") != -1) {
          this.sortType.key = `${type}-up`;
        } else {
          this.sortType.key = `${type}-down`;
        }
      }
    }
  },
  components: {
    listView,
    tileView,
    templateFilter,
    Pagination
  }
};
</script>

<style scoped>

.filter-container {
  background-color: rgba(72, 72, 72, 0.6);
  padding: 10px;
  border-radius: 5px;
}

.invisible {
  display: inline-block;
  height: 1px;
  width: 20px;
}

.filter-item {
  background-color: #e8e8e8;
  color: #666;
  margin-right: 5px;
  cursor: pointer;
  -moz-user-select: none;
  -khtml-user-select: none;
  user-select: none;
}

.filter-item:hover {
  background-color: #fff;
  color: #666;
}

.filter-item .uk-icon {
  margin-left: 5px;
  cursor: pointer;
}

.sort-button {
  cursor: pointer;
  -moz-user-select: none;
  -khtml-user-select: none;
  user-select: none;
  padding-left: 10px;
}

.toggle-button {
  padding: 0px;
  width: 50px;
}

.toggle-button.active,
.toggle-button.apply {
  background-color: rgba(0, 186, 182, 0.9);
  color: #fff;
  font-weight: bolder;
}

</style>
