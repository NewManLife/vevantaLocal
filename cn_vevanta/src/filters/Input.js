let input = {
  filters: {
    phoneSecret(v) {
      return v ? v.toString().replace(/(\d{3})(\d{3})(\d{2})(\d{2})/, '+7 ($1) $2-**-**') : v
    },

    phoneType(v) {
      return v ? v.toString().replace(/(\d{3})(\d{3})(\d{2})(\d{2})/, '+7 ($1) $2-$3-$4') : v
    },

    price(v) {
      if(!v) { return ''}
      v = v.toString().replace('.', ',')
      return v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ')
    },

    declination(count, val1, val2, val3) {
      let ct = Math.floor(count) % 10;
      if (
        (count % 100 > 10 && count % 100 < 20) ||
        ct >= 5 ||
        ct == 0
      )
        return `${count} ${val3}`;
      if (ct == 1) return `${count} ${val1}`;
      else if (ct > 1 && ct < 5) return `${count} ${val2}`;
    }
  }
}

export default input
