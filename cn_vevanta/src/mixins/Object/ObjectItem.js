let ObjectItem = {
  data() {
    return {
      dataByWall: {
        1: {
          name: 'Кирпичный дом'
        },
        2: {
          name: 'Монолитный дом'
        },
        3: {
          name: 'Панельный дом'
        },
        4: {
          name: 'Кирпичный дом'
        },
        5: {
          name: 'Блочный дом'
        },
        6: {
          name: 'Деревянный дом'
        },
        7: {
          name: 'Дом из бруса'
        },
      }
    }
  },
  methods: {
    title(item = null) {
      item = item || this.item;
      if (item.object.class_id == 1) {
        return item.object.object_type_flat_name || "Квартира";
      }
      if (item.object.class_id == 2) {
        return item.object.object_type_room_name || "Комната";
      }
      if (item.object.class_id == 3) {
        return item.house && item.house.wall_id ?
          this.dataByWall[item.house.wall_id].name :
          "Дом";
      }
      if (item.object.class_id == 4) {
        return item.object.object_type_room_name || "Гараж";
      }
      if (item.object.class_id == 5) {
        return item.object.object_type_commerce_name || "Помещение";
      }
      if (item.object.class_id == 6) {
        return `Участок ${item.object.object_type_area_name}`;
      }
    },
    getAddress(item = null) {
      let address = [];
      item = item || this.item;
      if (!!item.object.address_city_name) address.push(`г. ${item.object.address_city_name}`);

      if (
        !!item.object.address_settlement_name && [3, 6].includes(item.object.class_id)
      )
        address.push(`${item.object.address_settlement_name}`);

      if (
        !!item.object.address_tract_name &&
        (item.object.class_id == 6 || item.object.class_id == 3)
      )
        address.push(`${item.object.address_tract_name} тракт`);
        
      if (!!item.object.address_microdistrict_name)
        address.push(`мкр. ${item.object.address_microdistrict_name}`);

      if (!!item.object.address_street_name && [1, 2, 5].includes(item.object.class_id))
        address.push(`${item.object.address_street_name}`);

      if (
        !!item.object.address_house_name && [1, 2].includes(item.object.class_id)
      )
        address.push(`${item.object.address_house_name}`);

      return address.join(", ");
    }
  }
}

export default ObjectItem
