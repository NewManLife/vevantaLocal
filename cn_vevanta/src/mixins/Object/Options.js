let options = {
  data() {
    return {
      options: {
        action_id: [{
            id: 1,
            name: "Купить"
          },
          {
            id: 2,
            name: "Снять"
          }
        ],
        class_id: [{
            id: 1,
            name: "Квартира"
          },
          {
            id: 2,
            name: "Комната"
          },
          {
            id: 3,
            name: "Дом, дача, коттедж"
          },
          {
            id: 4,
            name: "Гараж"
          },
          {
            id: 5,
            name: "Коммерческая"
          },
          {
            id: 6,
            name: "Участок"
          }
        ],
        type_flat_id: [{
            id: 1,
            name: 'Студия'
          },
          {
            id: 2,
            name: "1 комнатная"
          },
          {
            id: 3,
            name: "2 комнатная"
          },
          {
            id: 4,
            name: "3 комнатная"
          },
          {
            id: 5,
            name: "4 комнатная"
          },
          {
            id: 6,
            name: '5 и более комнат'
          }
        ],
        type_building_id: [
          { id: 0, name: "Вторичка" },
          { id: 1, name: "Новостройка" }
        ],
        type_house_id: [
          { id: 1, name: "Отдельный дом" },
          { id: 2, name: "Часть дома" },
          { id: 3, name: "Таунхаус" },
          { id: 4, name: "Дуплекс" }
        ],
        house_wall_id: [
          { id: 1, name: "Кирпич" },
          { id: 2, name: "Монолит" },
          { id: 3, name: "Панель" },
          { id: 4, name: "Крипич-монолит" },
          { id: 5, name: "Блок" },
          { id: 6, name: "Дерево" },
          { id: 7, name: "Брус" }
        ],
        repairs_id: [
          { id: 1, name: "Косметический" },
          { id: 2, name: "Дизайнерский" },
          { id: 3, name: "Евро" },
          { id: 4, name: "Требуется" },
          { id: 5, name: "Улучшенный черновой" },
          { id: 6, name: "Черновой" },
          { id: 7, name: "Хороший" },
          { id: 8, name: "От застройщика" }
        ],
        tracts: [
          { id: 1, name: "Московский" },
          { id: 2, name: "Червишевский" },
          { id: 3, name: "Ирбитский" },
          { id: 4, name: "Салаирский" },
          { id: 5, name: "Старый Тобольский" },
          { id: 6, name: "Тобольский" },
          { id: 7, name: "Ялуторовский" },
          { id: 8, name: "Велижанский" }
        ]
      }
    }
  }
}
export default options
