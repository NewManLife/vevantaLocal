export default {
  computed: {
    isMobile() {
      return /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent);
    }
  },
  methods: {
    debounce(func, timeout = 1000) {
      let _timer, _timeout;
      return (args) => {
        if (_timeout && +new Date() - _timer <= timeout) clearTimeout(_timeout);
        _timeout = setTimeout(func, timeout, args);
        _timer = +new Date();
      };
    },
  }
}