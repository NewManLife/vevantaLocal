import metrika from './metrika.js'
let form = {
  mixins: [metrika],
  data() {
    return {
      api: {
        url: 'mail'
      },
      formName: '',
      msg: '',
      inputs: {
        phone: '',
        email: '',
        name: '',
      },
      dataRequest: {}
    }
  },

  methods: {
    sendForm() {
      if (this.inputs.phone || this.inputs.email) {
        let msg = this.msg
        this.msg = `Форма: ${this.formName} <br>Имя: ${this.inputs.name} <br>Телефон: ${this.formatPhone || this.inputs.phone} <br>E-mail: ${this.inputs.email} <br>Источник: ${location.href} <br>Комментарий: ${msg}`

        UIkit.modal(this.$root.$refs.modalDefault.$el).hide();
        setTimeout(() => {
          UIkit.modal(this.$root.$refs.modalSuccess.$el).show();
        }, 500)

        let dataRequest = {
          ...this.inputs,
          ...this.dataRequest,
          form: this.formName,
          msg: this.msg,
          url: location.href,
        }
        
        this.$axios.post(this.api.url, dataRequest)
        .then(() => {
          this.inputs.phone = '';
          this.inputs.email = '';
          this.inputs.name  = '';
          this.msg = '';
        })
      }
      this.metrikaJSYandex('form-zhdu-zvonka')
    },
  }
}

export default form
