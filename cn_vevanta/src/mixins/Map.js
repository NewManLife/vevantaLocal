
export default {
  data() {
    return {
      init: false,
      map: null,
      markers: null,
      districts: [],
      districtList: [],

      ids: [],
      showFilter: false,
      maxZoom: 18,
      center: [57.167136, 65.612397],
      selectedObjectsId: [],
      selectedObjects: [],
      showList: false,

      selectedMarker: null
    }
  },
  created() {
    this.debounceGetObjects = this.debounce(this.getObjectsByFilter, 1000);
    this.getMicrodistricts();
    this.getStorages();

    if (!this.isMobile) this.showFilter = true;
  },
  mounted() {
    this.getAllObjects()
      .then(res => {
        this.ids = res.data;

        if (this.lite_mode) {
          this.initLiteMap();
          return;
        }
        return this.initMap();
      })
      .then(() => {
        if (this.lite_mode) return;

        this.addMarkers();
        this.addDistricts();

        if (this.coords && this.coords.geo_lat && this.coords.geo_lon) {
          let selectedMarker = this.markers.getLayers().filter(layer => {
            let latlng = layer.getLatLng();
            return (
              latlng.lat == this.coords.geo_lat &&
              latlng.lng == this.coords.geo_lon
            );
          });

          if (selectedMarker.length > 0) {
            this.selectedObjectsId = selectedMarker.map(
              marker => marker.options.item.id
            );
            if (selectedMarker.length == 1)
              this.selectMarker(selectedMarker[0]);
            else this.selectMarker(selectedMarker[0].__parent);
          } else {
            this.centerMapOnMarker({
              lat: this.coords.geo_lat,
              lon: this.coords.geo_lon
            });
          }
        }
        this.init = true;
      });
  },
  methods: {
    initMap() {
      return DG.then(() => {
        return DG.plugin(
          "https://unpkg.com/leaflet.markercluster@1.4.1/dist/leaflet.markercluster.js"
        );
      }).then(() => {
        let zoom = 11;

        if (this.coords && this.coords.geo_lat && this.coords.geo_lon) {
          this.center = [this.coords.geo_lat, this.coords.geo_lon];
          zoom = 18;
        }

        this.map = DG.map("map", {
          center: this.center,
          zoom,
          fullscreenControl: false,
          zoomControl: false
        });
      });
    },

    initLiteMap() {
      return DG.then(() => {
        if (this.coords && this.coords.geo_lat && this.coords.geo_lon) {
          this.center = [this.coords.geo_lat, this.coords.geo_lon];
        }

        this.map = DG.map("map", {
          center: this.center,
          zoom: 20
        });
        DG.marker(this.center).addTo(this.map);
      });
    },

    addMarkers() {
      this.removeMarkers();
      this.createMarkerCluster();

      this.ids.forEach(item => {
        let iconHTML = '<div class="marker-cluster-icon"><span>1</span></div>';
        if (item.price_min) iconHTML = `<div class="marker-cluster-icon"><span></span><div class="marker-cluster-price">От ${this.formatPrice(item.price_min)} ₽</div></div>`;
        let myIcon = DG.divIcon({
          html: iconHTML
        });

        if (item.geo_lat && item.geo_lon) {
          let marker = DG.marker([item.geo_lat, item.geo_lon], {
            icon: myIcon,
            item
          });
          this.markers.addLayer(marker);
        }
      });

      this.map.addLayer(this.markers);
      this.addListenersForMarkers();
    },

    addDistricts() {
      this.districts.forEach((district, i) => {
        let latlngs = district.coords;
        DG.polygon(latlngs, {
          color: this.getColor(i),
          opacity: 0.5,
          fill: true,
          fillOpacity: 0.3
        })
        .bindLabel(district.title, {
          className: "map-popup"
        })
        .addTo(this.map);
      });
    },

    getMicrodistricts() {
      this.$axios
        .post("https://vevanta.ru/api/objects/get_microdistricts/", {})
        .then(res => {
          this.districtList = res.data.microdistricts.sort((a, b) => {
            if (a.name > b.name) return 1;
            if (a.name < b.name) return -1;
            return 0;
          });
          this.districts = res.data.microdistricts_coords;
        });
    },

    getObjectsByFilter() {
      this.getAllObjects().then(res => {
        this.ids = res.data;

        this.addMarkers();

        if (this.markers.getLayers().length > 0) {
          this.map.fitBounds(this.markers.getBounds());
        }
      });
    },

    zoomOrShowList(e) {
      let cluster = e.layer,
        bottomCluster = cluster;

      while (bottomCluster._childClusters.length === 1) {
        bottomCluster = bottomCluster._childClusters[0];
      }
      if (
        bottomCluster._zoom === 18 &&
        bottomCluster._childCount === cluster._childCount
      ) {
        return true;
      } else {
        cluster.zoomToBounds();
        return false;
      }
    },

    getColor(i) {
      let k = 360 / this.districts.length;
      return `hsla(${i * k}, 100%, 50%, .5)`;
    },

    createMarkerCluster() {
      this.markers = DG.markerClusterGroup({
        spiderfyOnMaxZoom: false,
        zoomToBoundsOnClick: false,

        iconCreateFunction: function (cluster) {
          return L.divIcon({
            html: `<div class='marker-cluster-icon'><span>${cluster.getChildCount()}</span></div>`
          });
        }
      });
    },

    removeMarkers() {
      if (this.markers) {
        this.map.removeLayer(this.markers);
        this.markers = null;
      }
    },

    addListenersForMarkers() {
      this.markers.on("clusterclick", a => {
        if (this.zoomOrShowList(a)) {
          this.selectedObjectsId = a.layer
            .getAllChildMarkers()
            .map(marker => marker.options.item.id);

          this.selectMarker(a.layer);
        }
      });

      this.markers.on("click", a => {
        this.selectedObjectsId = [a.layer.options.item.id];
        this.selectMarker(a.layer);
      });
    },

    selectMarker(layer) {
      this.getObjectsById().then(res => {
        this.selectedObjects = res.data;
        if (res.data.items) this.selectedObjects = res.data.items;
        this.showList = true;
        this.centerMapOnMarker(layer.getLatLng());
        this.toggleSelectMarker(layer._icon.children[0]);
      });
    },

    centerMapOnMarker(latLng) {
      this.map.setView(latLng);
    },

    toggleSelectMarker(elem) {
      if (this.selectedMarker) this.selectedMarker.classList.remove("selected");
      if (elem) {
        this.selectedMarker = elem;
        this.selectedMarker.classList.add("selected");
      }
    },

    formatPrice(v) {
      if(!v) { return ''}
      v = v.toString().replace('.', ',')
      return v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ')
    },
  }
}
