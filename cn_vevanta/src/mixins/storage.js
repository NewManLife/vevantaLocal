export default {
  data() {
    return {
      favoriteItems: [],
      newFavoriteItems: [],
    }
  },
  methods: {
    isAdded(id) {
      return {
        favorite: this.favoriteItems.includes(Number(id)),
        newFavorite: this.newFavoriteItems.includes(Number(id))
      };
    },

    addToStorage(id, storageKey) {
      if (this[storageKey].includes(Number(id))) {
        let index = this[storageKey].indexOf(Number(id));
        if (index != -1) this[storageKey].splice(index, 1);
        localStorage.setItem(storageKey, JSON.stringify(this[storageKey]));
        return;
      }

      this[storageKey].push(Number(id));
      localStorage.setItem(storageKey, JSON.stringify(this[storageKey]));
    },

    getStorages() {
      let favoriteItems = JSON.parse(localStorage.getItem("favoriteItems"));
      if (!!favoriteItems) this.favoriteItems = favoriteItems;

      let newFavoriteItems = JSON.parse(localStorage.getItem("newFavoriteItems"));
      if (!!newFavoriteItems) this.newFavoriteItems = newFavoriteItems;
    }
  }
}
