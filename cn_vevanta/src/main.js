
import Vue from 'vue'
import store from './store'
import axios from 'axios'

import FilterItems from './components/FilterItems.vue'
import ListingItems from './components/ListingItems.vue'

import ButtonModal from './components/ButtonModal.vue'
import Ipoteka from './components/Ipoteka.vue'
import IpotekaCalc from './components/IpotekaCalc'
import ipotekaForm from './components/ipotekaForm'

import QuizApartmentParameters from './components/QuizApartmentParameters.vue'
import QuizCountryHouseParameters from './components/QuizCountryHouseParameters'
import ManagerItem from './components/ManagerItem.vue'
import SearchFilterHome from './components/SearchFilterHome'
import SearchFilterMobile from './components/SearchFilterMobile'
import Consultation from './components/Consultation'
import ModalDefault from './components/ModalDefault'
import ModalSuccess from './components/ModalSuccess'
import FormPhone from './components/FormPhone'
import FormTwo from './components/FormTwo'
import DeveloperMap from './components/DeveloperMap'
import Personal from './components/Personal'

import PlansNewHouse from './components/NewHouse/Plans'
import NewHouseList from './components/NewHouse/List'
import NewHouseCard from './components/NewHouse/NewHouseCard'
import nwTopList from './components/NewHouse/nwTopList'

import OrderQuiz from './components/orderQuiz'
import CalcHouse from './components/CalcHouse'

import ObjectHelper from './components/ObjectHelper'
import ObjectsByParameters from './components/ObjectsByParameters'

import CallBackHero from './components/CallBackHero'

import lifeHouses from './components/lifeHouses'
import HousesMap from './components/map/Objects/HousesMap.vue'
import NewHousesMap from './components/map/newObjects/HousesMap.vue'

import ObjectsByAgent from './components/ObjectsByAgent.vue'
import AgentCallbackForm from './components/AgentCallbackForm.vue'
import Feedback from './components/Feedback.vue'
import ModalFeedback from './components/ModalFeedback.vue'

import FavoritePage from './components/favoritePage/Index.vue'

import BanksCards from './components/BanksCards.vue'
import BottomForm from './components/BottomForm.vue'
import CallBackConsultation from './components/CallBackConsultation.vue'
import CardsPartners from './components/CardsPartners.vue'
import ForWhat from './components/ForWhat.vue'
import HowItWorks from './components/HowItWorks.vue'
import IpotekaTop from './components/IpotekaTop.vue'
import ModalPartners from './components/ModalPartners.vue'
import ModalWorksAdv from './components/ModalWorksAdv.vue'
import Testimonials from './components/Testimonials.vue'
import VevantaVideo from './components/VevantaVideo.vue'
import VevantaVideoGallery from './components/VevantaVideoGallery.vue'
import WorksAdv from './components/WorksAdv.vue'
import NineCards from './components/NineCards.vue'
import HeroTop from './components/HeroTop.vue'
import NavLeft from './components/NavLeft.vue'
import CardWithIcon from './components/CardWithIcon.vue'
import HeroMiddle from './components/HeroMiddle.vue'
import ImgTextBlock from './components/ImgTextBlock.vue'
import MortgageSpec from './components/MortgageSpec.vue'
import MortgagePrograms from './components/MortgagePrograms.vue'
import MortgageList from './components/MortgageList.vue'
import HeroBottom from './components/HeroBottom.vue'
import FiveCards from './components/FiveCards.vue'
import BankPartners from './components/BankPartners.vue'
import IpotekaCalcStr from './components/IpotekaCalcStr.vue'

import ProjectListFilter from './components/ProjectListFilter.vue'
import ProjectsListSort from './components/ProjectsListSort.vue'
// import Projects from './components/Projects.vue'
// import ProjectList from './components/ProjectList.vue'
import ProjectItem from './components/ProjectItem.vue'







import Galery from './components/Galery'

Vue.prototype.$axios = axios.create({
 baseURL: 'https://vevanta.ru/api/'
})

Vue.directive('price-mask', {
  inserted(el, binding, vnode) {
    //изменяем v-model поля для отключения привязки
    let input = document.createElement('input');
    vnode.elm = input;

    el.addEventListener('input', (e) => {
      let model = vnode.data.directives.find(directive => directive.name == 'model');  //получаем v-model поля

      //ищем v-model с учетом вложенности
      let context = vnode.context;
      let expression = model.expression.split('.');
      while (expression.length > 1) {
        context = context[expression.splice(0, 1)];
      }
      expression = expression[0];

      //форматирование
      let val = e.target.value.replace(/[\D]/gi, "");
      e.target.value = val.length > 3 ? val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ') : val;
      context[expression] = val;
    })
  }
})

Vue.config.productionTip = false
let vm = new Vue({
  store,
  el: '#app',
  components: {
    ObjectHelper, OrderQuiz, HousesMap, CallBackHero, IpotekaCalcStr,
    ModalDefault, ModalSuccess, QuizCountryHouseParameters,
    FilterItems, ListingItems, lifeHouses, AgentCallbackForm, ObjectsByAgent, Feedback, ModalFeedback,
    FormPhone, FormTwo, PlansNewHouse, ipotekaForm, FiveCards,
    ButtonModal, Ipoteka, IpotekaCalc, ManagerItem, QuizApartmentParameters,MortgageSpec,MortgagePrograms,MortgageList,
    SearchFilterHome, SearchFilterMobile, nwTopList, HeroBottom,
    Consultation, DeveloperMap, ObjectsByParameters, FavoritePage,
    Personal, CalcHouse, NewHouseList, NewHouseCard, Galery, BankPartners,

    ProjectListFilter,ProjectsListSort,ProjectItem,

    BanksCards, BottomForm, CallBackConsultation, CardsPartners, ForWhat, HowItWorks, IpotekaTop, ModalPartners, ModalWorksAdv, Testimonials,
    VevantaVideo, VevantaVideoGallery, WorksAdv, NineCards, NewHousesMap, HeroTop, NavLeft, CardWithIcon, HeroMiddle, ImgTextBlock
  }
}).$mount()
